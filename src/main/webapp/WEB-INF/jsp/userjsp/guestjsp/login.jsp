<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="locale" content="${applicationScope.locale}">
    <link href="/css/css.css" rel="stylesheet">
    <title>Home</title>
    <script type="text/javascript">
        <jsp:include page="/js/jquery/jquery.js"/>
        <jsp:include page="/js/jquery/nav.js"/>
    </script>
</head>
<body>
<div class="wrapper">
    <header class="header">
        <jsp:include page="../../includejsp/header/header.jsp"/>
    </header>

    <main class="content">
        <jsp:include page="../../includejsp/section/loginSection.jsp"/>
    </main>
    <footer class="footer">
        <jsp:include page="../../includejsp/footer/footer.jsp"/>
    </footer>
</div>
</body>
</html>