<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="locale" content="${applicationScope.locale}">
    <link href="/css/css.css" rel="stylesheet">
    <title>User account</title>
    <script type="text/javascript">
        <jsp:include page="/js/jquery/jquery.js"/>
        <jsp:include page="/js/jquery/nav.js"/>
        <jsp:include page="/js/jquery/tabs.js"/>
        <jsp:include page="/js/jquery/datatables/dataTables.js"/>
        <jsp:include page="/js/jquery/datatables/sportSort.js"/>
        <jsp:include page="/js/jquery/doAction.js"/>
    </script>
</head>
<body>
<div class="wrapper">
    <header class="header">
        <jsp:include page="../includejsp/header/header.jsp"/>
    </header>
    <main class="content">
        <jsp:include page="../includejsp/section/userAccountSection.jsp"/>
    </main>
    <footer class="footer">
        <jsp:include page="../includejsp/footer/footer.jsp"/>
    </footer>
</div>
</body>
</html>