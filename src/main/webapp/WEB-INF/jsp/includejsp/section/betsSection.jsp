<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<div class="tab-container" id="tab-four">
    <span><fmt:message key="userAccount.bets"/> </span>
    <ctg:bets-table bets="${sessionScope.bets}" userRole="${sessionScope.userRole}" locale="${sessionScope.locale}"/>
</div>
${status}