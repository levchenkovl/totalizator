<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>


<div class="tab-container" id="tab-three">
    <span><fmt:message key="sport.coeffs"/> </span>
    <table class="table-data-tables display">
        <thead>
        <tr>
            <th>ID</th>
            <th><fmt:message key="sport.nameSport"/></th>
            <th><fmt:message key="sport.members"/></th>
            <c:forEach var="categoryCoeff" items="${categoryCoeffs}" varStatus="varStatus">
                <th data-orderable="false"><fmt:message key="sport.${categoryCoeff.name}"/></th>
            </c:forEach>
            <c:if test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                <th><fmt:message key="sport.nameCreatorMatch"/></th>
                <th data-orderable="false"></th>
                <th data-orderable="false"></th>
                <th data-orderable="false"></th>
            </c:if>
            <th data-orderable="false"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="sport" items="${sessionScope.coefficients}" varStatus="varStatus">
            <tr>
                <td>
                        ${sport.match.id}
                </td>
                <td>
                    <div>
                        <span>
                            <fmt:message key="sport.${sport.match.sportName}"/>
                        </span>
                    </div>
                </td>
                <td>
                    <div>
                        <span>
                            ${sport.match.ownerName} - ${sport.match.guestName}
                        </span>
                        <c:if test="${sport.match.isFinished()}">
                            <br/>
                            (${sport.match.ownerResult}:${sport.match.guestResult})
                        </c:if>
                    </div>
                </td>
                <c:choose>
                    <c:when test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                        <c:forEach var="i" begin="0" end="2" step="1">
                            <td>
                                <div>
                                <span>${sport.coefficients[i].value}
                                    |
                                        ${sport.coefficients[i].nameCreator}</span>
                                </div>
                            </td>
                        </c:forEach>
                        <td>
                            <c:if test="${sport.coefficients.size()-1 >= 3}">
                                <c:forEach var="i" begin="3" end="${sport.coefficients.size()-1}" step="1">
                                    <div>
                                            ${sport.coefficients[i].expOwner} : ${sport.coefficients[i].expGuest} |
                                            ${sport.coefficients[i].value}
                                        |
                                            ${sport.coefficients[i].nameCreator}
                                    </div>
                                </c:forEach>
                            </c:if>
                        </td>
                    </c:when>
                    <c:when test="${(sessionScope.userRole eq 'BOOKMAKER')}">
                        <c:forEach var="i" begin="0" end="2" step="1">
                            <td>
                                <button onclick="editCoeff(this, '${sport.coefficients[i].categoryCoeff}', '${sport.coefficients[i].id}','${sport.coefficients[i].value}')"
                                        type="button">${sport.coefficients[i].value}</button>

                                |
                                    ${sport.coefficients[i].nameCreator}
                            </td>
                        </c:forEach>
                        <td>
                            <c:if test="${sport.coefficients.size()-1 >= 3}">
                                <c:forEach var="i" begin="3" end="${sport.coefficients.size()-1}" step="1">
                                    <div>
                                            ${sport.coefficients[i].expOwner} : ${sport.coefficients[i].expGuest} |
                                        <button onclick="editResCoeff(this, '${sport.coefficients[i].categoryCoeff}',
                                                '${sport.coefficients[i].id}','${sport.coefficients[i].value}',
                                                '${sport.coefficients[i].expOwner}','${sport.coefficients[i].expGuest}')"
                                                type="button">${sport.coefficients[i].value}</button>
                                        |
                                            ${sport.coefficients[i].nameCreator}
                                    </div>
                                </c:forEach>
                            </c:if>
                        </td>
                    </c:when>
                    <c:when test="${sessionScope.userRole eq 'CLIENT'}">
                        <c:forEach var="i" begin="0" end="2" step="1">
                            <td>
                                <div>
                                    <c:if test="${sport.coefficients[i] ne null and sport.coefficients[i].value ne 1.0}">
                                        <button onclick="doBet(this, '${sport.coefficients[i].categoryCoeff}',
                                                '${sport.coefficients[i].id}', '${sport.coefficients[i].value}')"
                                                type="button">
                                                ${sport.coefficients[i].value}
                                        </button>
                                    </c:if>
                                </div>
                            </td>
                        </c:forEach>
                        <td>
                            <c:if test="${sport.coefficients.size()-1 >= 3}">
                                <c:forEach var="i" begin="3" end="${sport.coefficients.size()-1}" step="1">
                                    <div>
                                        <c:if test="${sport.coefficients[i] ne null and sport.coefficients[i].value ne 1.0}">
                                            ${sport.coefficients[i].expOwner} : ${sport.coefficients[i].expGuest} |
                                            <button onclick="doBet(this, '${sport.coefficients[i].categoryCoeff}',
                                                    '${sport.coefficients[i].id}', '${sport.coefficients[i].value}')"
                                                    type="button">
                                                    ${sport.coefficients[i].value}
                                            </button>
                                        </c:if>
                                    </div>
                                </c:forEach>
                            </c:if>
                        </td>
                    </c:when>
                    <c:when test="${sessionScope.userRole eq null}">
                        <c:forEach var="i" begin="0" end="2" step="1">
                            <td>
                                <div>
                                    <c:if test="${sport.coefficients[i] ne null and sport.coefficients[i].value ne 1.0}">
                                        <a href="/controller?command=define_page&page=login">${sport.coefficients[i].value}</a>
                                    </c:if>
                                </div>
                            </td>
                        </c:forEach>
                        <td>
                            <c:if test="${sport.coefficients.size()-1 >= 3}">
                                <c:forEach var="i" begin="3" end="${sport.coefficients.size()-1}" step="1">
                                    <div>
                                        <c:if test="${sport.coefficients[i] ne null and sport.coefficients[i].value ne 1.0}">
                                            ${sport.coefficients[i].expOwner} : ${sport.coefficients[i].expGuest} |
                                            <a href="/controller?command=define_page&page=login">${sport.coefficients[i].value}</a>
                                        </c:if>
                                    </div>
                                </c:forEach>
                            </c:if>
                        </td>
                    </c:when>
                </c:choose>
                <c:if test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                    <td>
                            ${sport.match.nameCreator}
                    </td>
                    <td>
                        <button class="input-image edit" onClick="edit(this)" type="button"/>
                    </td>
                    <td>
                        <form method="post" action="/controller">
                            <input type="hidden" name="sessId" value="${sessionScope.sessId}">
                            <input type="hidden" name="page" value="${pageContext.request.requestURL}">
                            <input type="hidden" name="command" value="delete_sport">
                            <input type="hidden" name="matchId" value="${sport.match.id}">
                            <button class="input-image delete" type="button" value=""
                                    onclick="deleteMatch(this, '<fmt:message
                                            key="sport.sport.delete"/>')"/>
                        </form>
                    </td>
                    <td>
                        <form method="post" action="/controller">
                            <input type="hidden" name="sessId" value="${sessionScope.sessId}">
                            <input type="hidden" name="command" value="result_match">
                            <input type="hidden" name="matchId" value="${sport.match.id}">
                            <input class="input-image " type="submit" value="<fmt:message
                                key="sport.sport.result"/>">
                        </form>
                    </td>
                </c:if>
                <td>
                    <%@ include file="popupWin.jsp" %>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <c:if test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
        <button type="button" onclick="addMatch(this)"><fmt:message key="sport.addNewMatch"/></button>
        <jsp:include page="addNewMatchSection.jsp"/>
    </c:if>
</div>
${status}