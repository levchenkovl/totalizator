<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<form id="register-form" method="post" action="/controller">
    <input type="hidden" name="sessId" value="${sessionScope.sessId}">
    <input type="hidden" name="command" value="send_register"/>
    <fmt:message key="users.username"/> : <br/>
    <input type="text" name="username" value="" required
           oninvalid="this.setCustomValidity('<fmt:message key="users.patternUsername"/>')"
           oninput="this.setCustomValidity('')"
           pattern="^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$">
    <br/>
    <fmt:message key="users.password"/>:<br/>
    <input type="password" name="password" value="" required
           oninvalid="this.setCustomValidity('<fmt:message key="users.patternPassword"/>')"
           oninput="this.setCustomValidity('')"
           pattern="^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S{8,}$">
    <br/>
    <fmt:message key="users.email"/>:<br/>
    <input type="email" name="email" value="" required>
    <br/>
    <c:if test="${sessionScope.userRole ne null and sessionScope.userRole eq 'ADMINISTRATOR'}">
        <fmt:message key="users.role"/>:
        <br/>
        <select name="role" required>
            <option disabled><fmt:message key="register.chooseRole"/></option>
            <option value="Administrator"><fmt:message key="register.administrator"/></option>
            <option value="Bookmaker"><fmt:message key="register.bookmaker"/></option>
            <option selected value="client"><fmt:message key="register.client"/></option>
        </select>
        <br/>
    </c:if>
    ${status}
    <br/>
    <input type="submit" value="<fmt:message key="register"/>">
</form>