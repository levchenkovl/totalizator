<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>

<p><fmt:message key="userAccount"/> ${sessionScope.subUser.username}</p>

<div id="tabs">
    <ul class="tabs-menu clearfix">
        <li class="active">
            <a href="#" data-id="tab-one"><fmt:message key="userAccount.pi"/></a>
        </li>
        <c:if test="${sessionScope.user.id eq sessionScope.subUser.id}">
            <li>
                <a href="#" data-id="tab-two"><fmt:message key="userAccount.secure"/></a>
            </li>
        </c:if>
        <c:if test="${sessionScope.subUser.role eq 'BOOKMAKER'}">
            <li>
                <a href="#" data-id="tab-three"><fmt:message key="userAccount.coeffs"/></a>
            </li>
        </c:if>
        <c:if test="${sessionScope.subUser.role eq 'ADMINISTRATOR'}">
            <li>
                <a href="#" data-id="tab-three"><fmt:message key="userAccount.matchs"/></a>
            </li>
        </c:if>
        <c:if test="${sessionScope.subUser.role eq 'CLIENT'}">
            <li>
                <a href="#" data-id="tab-four"><fmt:message key="userAccount.bets"/></a>
            </li>
        </c:if>
    </ul>

    <div class="tab-container" id="tab-one">
        <span>Игровой счет :</span><span>${sessionScope.subUser.id}</span><br/>
        <span>Владелец счета :</span><span>${sessionScope.subUser.username}</span><br/>
        <span><fmt:message key="users.email"/> :</span><span>${sessionScope.subUser.email}</span><br/>
        <c:if test="${sessionScope.userRole eq 'CLIENT'}">
            <span><fmt:message key="users.balance"/> :</span><span>${sessionScope.subUser.balance}</span><br/>
        </c:if>
        <c:if test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
            <span><fmt:message key="users.role"/> :</span><span>${sessionScope.subUser.role}</span><br/>
        </c:if>

        <c:if test="${sessionScope.user.id ne sessionScope.subUser.id}">
            <div>
                <form method="post" action="/controller">
                    <input type="hidden" name="sessId" value="${sessionScope.sessId}">
                    <input type="hidden" name="command" value="delete_user">
                    <input type="hidden" name="userId" value="${sessionScope.subUser.id}">
                    <input type="hidden" name="username" value="${sessionScope.subUser.username}">
                    <input type="hidden" name="role" value="${sessionScope.subUser.role}">
                    <input type="submit" value="<fmt:message key="userAccount.delete"/>"/>
                </form>
            </div>
        </c:if>
    </div>

    <c:if test="${sessionScope.user.id eq sessionScope.subUser.id}">
        <div class="tab-container" id="tab-two">
            <form method="post" action="/controller">
                <input type="hidden" name="sessId" value="${sessionScope.sessId}">
                <input type="hidden" name="command" value="change_password"><br/>
                <span><fmt:message key="currentPassword"/> </span><input type="password" name="current_password"
                                                                         value=""><br/>
                <span><fmt:message key="newPassword"/></span>
                <input type="password" name="new_password" value="" required
                       oninvalid="this.setCustomValidity('<fmt:message key="users.patternPassword"/>')"
                       oninput="this.setCustomValidity('')"
                       pattern="^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S{8,}$">
                <br/>
                <span><fmt:message key="againPassword"/></span>
                <input type="password" name="again_password" value="" required
                       oninvalid="this.setCustomValidity('<fmt:message key="users.patternPassword"/>')"
                       oninput="this.setCustomValidity('')"
                       pattern="^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S{8,}$">
                <br/>
                <input type="submit" value="<fmt:message key="updatePassword"/>">
            </form>
        </div>
    </c:if>

    <c:if test="${sessionScope.subUser.role eq 'BOOKMAKER' or sessionScope.subUser.role eq 'ADMINISTRATOR'}">
        <c:import url="/WEB-INF/jsp/includejsp/section/sportSection.jsp"/>
    </c:if>
    <c:if test="${sessionScope.subUser.role eq 'CLIENT'}">
        <c:import url="/WEB-INF/jsp/includejsp/section/betsSection.jsp"/>
    </c:if>
</div>