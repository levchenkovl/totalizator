<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>
<div id="chat">
    <c:choose>
        <c:when test="${sessionScope.user ne null}">
            <textarea readonly="readonly" id="messages" rows="45"><fmt:message
                    key="chatHello"/>, ${sessionScope.user.username}!</textarea>
            <hr/>
            <textarea id="message" cols="22" rows="2"></textarea>
            <input id="send-message" value="<fmt:message key="sendMessage"/>" type="button"/>
        </c:when>
        <c:otherwise>

        </c:otherwise>
    </c:choose>
</div>