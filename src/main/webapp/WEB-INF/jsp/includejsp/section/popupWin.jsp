<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>

<div class="win" style="display:none;">
    <div class="overlay"></div>
    <div class="visible">
        <div class="content-popup">
            <form method="post" action="/controller">
                <input type="hidden" name="sessId" value="${sessionScope.sessId}">
                <input type="hidden" name="matchId" value="${sport.match.id}">
                <c:choose>
                    <c:when test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                        <input type="hidden" name="command" value="edit_sport">
                    </c:when>
                    <c:when test="${sessionScope.userRole eq 'CLIENT'}">
                        <input type="hidden" name="command" value="add_bet">
                    </c:when>
                </c:choose>
                <input type="hidden" name="page" value="${pageContext.request.requestURL}">
                <table class="display">
                    <thead>
                    <tr>
                        <th><fmt:message key="sport.nameSport"/></th>
                        <th><fmt:message key="sport.members"/></th>

                        <c:if test="${sessionScope.userRole eq 'BOOKMAKER'}">
                            <th class="expOwner"><fmt:message key="sport.ownerRes"/></th>
                            <th class="expGuest"><fmt:message key="sport.guestRes"/></th>
                        </c:if>

                        <c:if test="${sessionScope.userRole ne 'ADMINISTRATOR'}">
                            <c:forEach var="coeffic" items="${categoryCoeffs}"
                                       varStatus="varStatus">
                                <th class=" winner ${coeffic.name}"><fmt:message
                                        key="sport.${coeffic.name}"/></th>
                            </c:forEach>
                        </c:if>
                        <c:choose>
                            <c:when test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                                <th><fmt:message key="sport.nameCreatorMatch"/></th>
                            </c:when>
                            <c:when test="${sessionScope.userRole eq 'BOOKMAKER'}">
                                <th></th>
                            </c:when>
                            <c:when test="${sessionScope.userRole eq 'CLIENT'}">
                                <th><fmt:message key="sport.bet"/></th>
                            </c:when>
                        </c:choose>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            ${sport.match.id}
                        </td>
                        <td>
                            <div>
                                <c:choose>
                                    <c:when test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                                        <select name="sportName" required>
                                            <option disabled><fmt:message key="sport.chooseSport"/></option>
                                            <option selected value="football"><fmt:message
                                                    key="sport.football"/></option>
                                            <option value="basketball"><fmt:message key="sport.basketball"/></option>
                                            <option value="volleyball"><fmt:message key="sport.volleyball"/></option>
                                            <option value="mini-football"><fmt:message
                                                    key="sport.miniFootball"/></option>
                                            <option value="handball"><fmt:message key="sport.handball"/></option>
                                            <option value="hockey"><fmt:message key="sport.hockey"/></option>
                                        </select>
                                    </c:when>
                                    <c:when test="${sessionScope.userRole eq 'BOOKMAKER' or sessionScope.userRole eq 'CLIENT'}">
                                                            <span>
                                                                    ${sport.match.sportName}
                                                            </span>
                                    </c:when>
                                </c:choose>
                            </div>
                        </td>
                        <td>
                            <div>
                                <c:choose>
                                    <c:when test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                                        <input type="text" required name="owner"
                                               value="${sport.match.ownerName}"/>
                                        <input type="text" required name="guest"
                                               value="${sport.match.guestName}"/>
                                    </c:when>
                                    <c:when test="${sessionScope.userRole eq 'BOOKMAKER' or sessionScope.userRole eq 'CLIENT'}">
                                                            <span>
                                                            ${sport.match.ownerName} - ${sport.match.guestName}
                                                            </span>
                                    </c:when>
                                </c:choose>
                                <c:if test="${sport.match.isFinished()}">
                                    <br/>
                                    (${sport.match.ownerResult}:${sport.match.guestResult})
                                </c:if>
                            </div>
                        </td>

                        <c:if test="${sessionScope.userRole eq 'BOOKMAKER'}">
                            <td class="expOwner">
                                <div></div>
                            </td>
                            <td class="expGuest">
                                <div></div>
                            </td>
                        </c:if>


                        <c:if test="${sessionScope.userRole ne 'ADMINISTRATOR'}">
                            <c:forEach var="coeffic" items="${categoryCoeffs}"
                                       varStatus="varStatus">
                                <td class=" winner ${coeffic.name}">
                                    <div>
                                    </div>
                                </td>
                            </c:forEach>
                        </c:if>

                        <c:choose>
                            <c:when test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                                <td>
                                        ${sport.match.nameCreator}
                                </td>
                                <td>
                                    <input class="input-image save" type="button" value=""
                                           onclick="saveMatch(this,'<fmt:message
                                                   key="sport.sport.edit"/>')"/>
                                </td>
                            </c:when>
                            <c:when test="${sessionScope.userRole eq 'BOOKMAKER'}">
                                <td>
                                    <input class="input-image save" type="button" value=""
                                           onclick="saveCoeff(this,'<fmt:message
                                                   key="sport.coeff.edit"/>')"/>
                                </td>
                                <td>
                                    <input class="input-image delete" type="button" value=""
                                           onclick="deleteCoeff(this, '<fmt:message
                                                   key="sport.coeff.delete"/>')"/>
                                </td>
                            </c:when>
                            <c:when test="${sessionScope.userRole eq 'CLIENT'}">
                                <td>
                                    <input type="number" name="value" value="0" min="1"/>
                                </td>
                                <td>
                                    <button type="button" value=""
                                            onclick="saveMatch(this,'<fmt:message
                                                    key="sport.bet.do"/>')"><fmt:message
                                            key="sport.bet.do"/></button>
                                </td>
                            </c:when>
                        </c:choose>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <button type="button" onClick="$('.win').hide();"><fmt:message
                key="close"/>
        </button>
    </div>
</div>