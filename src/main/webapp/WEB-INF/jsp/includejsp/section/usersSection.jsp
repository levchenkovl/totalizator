<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>


<div id="users">
    <p>Пользователи.</p>
    <table class="table-data-tables display">
        <thead>
        <tr>
            <th>ID</th>
            <th><fmt:message key="users.username"/></th>
            <th><fmt:message key="users.email"/></th>
            <th><fmt:message key="users.role"/></th>
            <th><fmt:message key="users.balance"/></th>
            <th data-orderable="false"></th>
            <th data-orderable="false"></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th><fmt:message key="users.username"/></th>
            <th><fmt:message key="users.email"/></th>
            <th><fmt:message key="users.role"/></th>
            <th><fmt:message key="users.balance"/></th>
            <th></th>
            <th></th>
        </tr>
        </tfoot>
        <tbody>
        <c:forEach var="user" items="${sessionScope.users}" varStatus="varStatus">
            <tr>
                <td>
                        ${user.id}
                </td>
                <td>
                        ${user.username}
                </td>
                <td>
                        ${user.email}
                </td>
                <td>
                        ${user.role}
                </td>
                <td>
                        ${user.balance}
                </td>
                <c:choose>
                    <c:when test="${user.id ne sessionScope.user.id}">
                        <td>
                            <form method="post" action="/controller">
                                <input type="hidden" name="sessId" value="${sessionScope.sessId}">
                                <input type="hidden" name="command" value="edit_user">
                                <input type="hidden" name="userId" value="${user.id}">
                                <input class="input-image edit" type="submit" value=""/>
                            </form>
                        </td>
                        <td>
                            <form method="post" action="/controller">
                                <input type="hidden" name="sessId" value="${sessionScope.sessId}">
                                <input type="hidden" name="command" value="delete_user">
                                <input type="hidden" name="userId" value="${user.id}">
                                <input type="hidden" name="username" value="${user.username}">
                                <input type="hidden" name="role" value="${user.role}">
                                <input class="input-image delete" type="button" value=""
                                       onclick="deleteUser(this,'<fmt:message key="users.delete"/>')"/>
                            </form>
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td></td>
                        <td></td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<div>
    <jsp:include page="registerSection.jsp"/>
    <input type="button" value="<fmt:message key="users.addNewUser"/>"
           onclick="addUser(this)"/>
</div>
${status}<br/>