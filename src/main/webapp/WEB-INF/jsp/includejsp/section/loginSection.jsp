<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>

<div class="login-div">
    <form id="login-form" method="Post" action="/controller">
        <input type="hidden" name="sessId" value="${sessionScope.sessId}">
        <input type="hidden" name="command" value="login"/>
        <br/><span><fmt:message key="users.username"/>:</span><br/>
        <input type="text" name="username"/>
        <br/><fmt:message key="users.password"/>:<br/>
        <input type="password" name="password"/>
        <br/>
        ${status}
        <br/>
    </form>
    <input form="login-form" type="submit" value="Log in"/>
</div>