<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>

<c:choose>
    <c:when test="${sessionScope.userRole ne null and sessionScope.userRole eq 'ADMINISTRATOR'}">
        <div class="register-div" style="display:none;">
        <div class="overlay"></div>
        <div class="visible">
            <h2><fmt:message key="userRegistration"/></h2>
            <div class="content-popup">
                <jsp:include page="registerForm.jsp"/>
            </div>
            <button type="button" onClick="$('.register-div').hide();">закрыть
            </button>
        </div>
    </c:when>
    <c:otherwise>
        <div class="register-div">
            <jsp:include page="registerForm.jsp"/>
        </div>
    </c:otherwise>
</c:choose>
</div>