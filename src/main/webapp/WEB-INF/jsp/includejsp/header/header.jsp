<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="pagecontent"/>


<a href="/controller?command=define_page&page=home" class="logo">Totalizator</a>
<nav>
    <ul>
        <li><a href="/controller?command=define_page&page=home"><fmt:message key="header.home"/></a></li>
        <li><a href="/controller?command=print_sports"><fmt:message key="header.sport"/></a></li>
        <c:if test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
            <li><a href="/controller?command=print_users"><fmt:message key="header.users"/></a></li>
        </c:if>

        <c:if test="${sessionScope.user ne null}">
            <li><a href="/controller?command=user_account">${sessionScope.user.username}</a></li>
            <li><a href="/controller?command=logout&sessId=${sessionScope.sessId}"><fmt:message
                    key="header.logout"/></a></li>
        </c:if>
        <c:if test="${sessionScope.user eq null}">
            <li><a href="/controller?command=define_page&page=login"><fmt:message key="header.login"/></a></li>
            <li><a href="/controller?command=define_page&page=register"><fmt:message
                    key="header.register"/></a></li>
        </c:if>
        <li>
            <form class="localeForm" method="post" action="/controller">
                <input type="hidden" name="command" value="changelang">
                <input type="hidden" name="page" value="${pageContext.request.requestURL}">
                <input type="submit" id="r1" name="locale" value="RU">
                <input type="submit" id="r2" name="locale" value="EN">
                <input type="submit" id="r3" name="locale" value="BY">
            </form>
        </li>
    </ul>
</nav>

