function deleteUser(obj, q) {
    if (confirm(q)) {
        $(obj).parent().submit();
    } else {

    }
}

function deleteCoeff(obj, q) {
    $('.win form input[name=value]').remove();

    $('<input/>', {
        class: 'small-input',
        name: 'value',
        value: '1',
        type: 'hidden'
    }).appendTo($('.win form'));

    saveCoeff(obj, q)
}

function deleteMatch(obj, q) {
    if (confirm(q)) {
        $(obj).parent().submit();
    } else {

    }
}

function edit(obj) {
    $(obj).closest('tr').find('.win').removeAttr('style');
}

function addUser() {
    $('.register-div').removeAttr('style');
}

function addMatch() {
    $('.win.match').removeAttr('style');
}

function saveMatch(obj, q) {
    if (confirm(q)) {
        $(obj).closest('form').submit();
    }
}

function saveCoeff(obj, q) {
    $('.win form input[name=command]').remove();
    $('<input/>', {
        name: 'command',
        value: 'edit_coeff',
        type: 'hidden'
    }).appendTo($(obj).closest('form'));

    if (confirm(q)) {
        $(obj).closest('form').submit();
    }
}

function doBet(obj, winner, coeffId, value) {
    $(obj).closest('tr').find('.win').removeAttr('style');

    $('.winner').hide();
    var form = $('.win form');

    $('.win form input[name=coeffId]').remove();
    $('.winner.' + winner).show();

    $('<input/>', {
        name: 'coeffId',
        value: coeffId,
        type: 'hidden'
    }).appendTo(form);

    $('<span>' + value + '</span>').appendTo($('.winner.' + winner + ' div'));
}

function editResCoeff(obj, winner, coeffId, value, expOwner, expGuest) {
    editCoeff(obj, winner, coeffId, value);


    $('.expOwner').show();
    $('.expGuest').show();

    $('.win form input[name=expOwner]').remove();

    $('.win form input[name=expGuest]').remove();

    $('<input/>', {
        class: 'small-input',
        name: 'expOwner',
        value: expOwner,
        type: 'number',
        min: "0"
    }).appendTo($('.win .expOwner div'));

    $('<input/>', {
        class: 'small-input',
        name: 'expGuest',
        value: expGuest,
        type: 'number',
        min: "0"
    }).appendTo($('.win .expGuest div'));

}

function editCoeff(obj, winner, coeffId, value) {

    $('.expOwner').hide();
    $('.expGuest').hide();

    $(obj).closest('tr').find('.win').removeAttr('style');

    $('.winner').hide();
    var form = $('.win form');

    $('.win form input[name=coeffId]').remove();
    $('.winner.' + winner).show();

    $('<input/>', {
        name: 'coeffId',
        value: coeffId,
        type: 'hidden'
    }).appendTo(form);

    $('.win form input[name=categoryCoeff]').remove();

    $('.winner div input[name=value]').remove();

    $('<input/>', {
        class: 'small-input',
        name: 'value',
        value: value,
        type: 'number',
        min: "0"
    }).appendTo($('.win .winner.' + winner + ' div'));

}