package by.levchenko.controller.filter;

import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.CommandEnum;
import by.levchenko.controller.utility.UtilController;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A filter that protects against the re-execution of a query.
 *
 * @author Vladislav Levchenko
 */

@WebFilter(urlPatterns = {"/controller"},

        initParams = {@WebInitParam(name = "HOME_PATH", value = "/WEB-INF/jsp/userjsp/home.jsp")})
public class F5Filter implements Filter {
    private FilterConfig filterConfig;
    private String homePath;
    private List<CommandEnum> commands = new ArrayList<>();

    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        homePath = filterConfig.getInitParameter("HOME_PATH");

        commands.add(CommandEnum.PRINT_SPORTS);
        commands.add(CommandEnum.PRINT_USERS);
        commands.add(CommandEnum.LOGIN);
        commands.add(CommandEnum.CHANGELANG);
        commands.add(CommandEnum.USER_ACCOUNT);
        commands.add(CommandEnum.EDIT_USER);
        commands.add(CommandEnum.DEFINE_PAGE);
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        if (filterConfig == null) {
            return;
        }

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;


        request.getSession().removeAttribute("status");
        String sessId = request.getParameter("sessId");
        String action = request.getParameter("command");

        if (!commands.contains(CommandEnum.valueOf(action.toUpperCase())) && (sessId == null || !sessId.equals(String.valueOf(request.getSession().getAttribute("sessId"))))) {
            request.setAttribute("status",
                    MessageManager.getProperty("message.f5"));
            filterConfig.getServletContext().getRequestDispatcher(homePath).forward(request, response);
            return;
        }


        request.getSession().setAttribute("sessId", UtilController.getRandomSessId());

        filterChain.doFilter(servletRequest, servletResponse);

    }

    public void destroy() {

    }
}
