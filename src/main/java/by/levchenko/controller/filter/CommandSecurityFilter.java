package by.levchenko.controller.filter;


import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.CommandEnum;
import by.levchenko.command.utility.UtilCommand;
import by.levchenko.database.entity.UserRole;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A filter that protects against unauthorized access to commands.
 *
 * @author Vladislav Levchenko
 */

@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD},

        urlPatterns = {"/controller"},

        initParams = {@WebInitParam(name = "HOME_PATH", value = "/WEB-INF/jsp/userjsp/home.jsp")})
public class CommandSecurityFilter implements Filter {
    private FilterConfig filterConfig;
    private String homePath;
    private Map<UserRole, List<CommandEnum>> roleMap = new HashMap<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        homePath = filterConfig.getInitParameter("HOME_PATH");


        roleMap.put(UserRole.GUEST, Arrays.asList(
                CommandEnum.DEFINE_PAGE,
                CommandEnum.LOGIN,
                CommandEnum.CHANGELANG,
                CommandEnum.PRINT_SPORTS,
                CommandEnum.SEND_REGISTER,
                CommandEnum.REGISTER));

        roleMap.put(UserRole.CLIENT, Arrays.asList(
                CommandEnum.DEFINE_PAGE,
                CommandEnum.LOGOUT,
                CommandEnum.PRINT_SPORTS,
                CommandEnum.CHANGELANG,
                CommandEnum.USER_ACCOUNT,
                CommandEnum.CHANGE_PASSWORD,
                CommandEnum.REGISTER,
                CommandEnum.ADD_BET));

        roleMap.put(UserRole.BOOKMAKER, Arrays.asList(
                CommandEnum.DEFINE_PAGE,
                CommandEnum.LOGOUT,
                CommandEnum.PRINT_SPORTS,
                CommandEnum.CHANGELANG,
                CommandEnum.USER_ACCOUNT,
                CommandEnum.CHANGE_PASSWORD,
                CommandEnum.REGISTER,
                CommandEnum.EDIT_COEFF));

        roleMap.put(UserRole.ADMINISTRATOR, Arrays.asList(
                CommandEnum.DEFINE_PAGE,
                CommandEnum.LOGOUT,
                CommandEnum.PRINT_SPORTS,
                CommandEnum.CHANGELANG,
                CommandEnum.USER_ACCOUNT,
                CommandEnum.CHANGE_PASSWORD,
                CommandEnum.SEND_REGISTER,
                CommandEnum.REGISTER,
                CommandEnum.ADD_SPORT,
                CommandEnum.EDIT_SPORT,
                CommandEnum.DELETE_SPORT,
                CommandEnum.EDIT_USER,
                CommandEnum.DELETE_USER,
                CommandEnum.PRINT_USERS,
                CommandEnum.RESULT_MATCH));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        if (filterConfig == null) {
            return;
        }

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        request.getSession().removeAttribute("status");

        UserRole userRole = UtilCommand.defineUserRole(request);

        String action = request.getParameter("command");
        if (action != null && !action.isEmpty()) {
            try {
                CommandEnum command = CommandEnum.valueOf(action.toUpperCase());

                if (!roleMap.get(userRole).contains(command)) {
                    request.setAttribute("status",
                            MessageManager.getProperty("message.noAccess"));
                    filterConfig.getServletContext().getRequestDispatcher(homePath).forward(request, response);
                    return;
                }
            } catch (IllegalArgumentException e) {
                request.setAttribute("status", MessageManager.getProperty("message.wrongaction"));
                filterConfig.getServletContext().getRequestDispatcher(homePath).forward(request, response);
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
