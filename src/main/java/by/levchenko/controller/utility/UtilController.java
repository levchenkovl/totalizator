package by.levchenko.controller.utility;

import java.util.Date;
import java.util.Random;

/**
 * Methods for the controller.
 *
 * @author Vladislav Levchenko
 */

public class UtilController {

    /**
     * Generation session id for F5Filter.
     *
     * @return random session id.
     */
    public static long getRandomSessId() {
        long sessId = 0;
        sessId = new Random(new Date().getTime()).nextInt();
        return sessId;
    }
}
