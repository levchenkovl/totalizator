package by.levchenko.controller.listener;


import by.levchenko.controller.utility.UtilController;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class SessionListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent sessionEvent) {
        sessionEvent.getSession().setAttribute("sessId", UtilController.getRandomSessId());
    }

    public void sessionDestroyed(HttpSessionEvent sessionEvent) {

    }

}