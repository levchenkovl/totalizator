package by.levchenko.controller.socket;

import by.levchenko.database.entity.User;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * A WebSocket server that provides chat support.
 *
 * @author Vladislav Levchenko
 */

@ServerEndpoint(value = "/chat", configurator = GetHttpSessionConfigurator.class)
public class Chat {

    /**
     * WebSocket sessions of users.
     */
    private static ConcurrentLinkedQueue<Session> wsSessions = new ConcurrentLinkedQueue<>();

    /**
     * Number of messages displayed first.
     */
    private final static int NUMBER_OF_MESSAGES = 100;

    /**
     * Messages displayed first.
     */
    private static ConcurrentLinkedQueue<String> messages = new ConcurrentLinkedQueue<String>() {
        /**
         * Inserts the specified element at the tail of this queue.
         * As the queue is unbounded, this method will never throw
         * {@link IllegalStateException} or return {@code false}.
         *
         * @param s
         * @return {@code true} (as specified by )
         * @throws NullPointerException if the specified element is null
         */
        @Override
        public boolean add(String s) {
            while (this.size() >= NUMBER_OF_MESSAGES) {
                super.remove();
            }
            return super.add(s);
        }
    };
    private HttpSession httpSession;

    @OnOpen
    public void open(Session session, EndpointConfig config) {
        wsSessions.add(session);
        httpSession = (HttpSession) config.getUserProperties()
                .get(HttpSession.class.getName());
        try {
            for (String message : messages) {
                session.getBasicRemote().sendText(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void eventClose(Session session) {
        wsSessions.remove(session);
    }

    @OnMessage
    public void EventMessage(String message, Session session) {
        User user = (User) httpSession.getAttribute("user");
        if (user == null) {
            return;
        }

        message = message.replaceAll("\\s+", " ");

        SimpleDateFormat formatForDateNow = new SimpleDateFormat("HH:mm:ss");
        String reply = formatForDateNow.format(new Date()) + " " + user.getUsername() + ": " + message;

        try {
            if (messages.add(reply)) {
                for (Session ses : wsSessions) {
                    ses.getBasicRemote().sendText(reply);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnError
    public void eventError(Throwable t) {
        System.err.println("Error WebSocket");
    }
}
