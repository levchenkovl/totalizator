package by.levchenko.database.connectionpool;

import by.levchenko.database.dao.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A singleton class that controls connections to the database.
 *
 * @author Vladislav Levchenko
 */

public class ConnectionPool {

    private static Logger Logger = LogManager.getLogger(ConnectionPool.class);

    private static ReentrantLock lock = new ReentrantLock();
    private static AtomicBoolean isActive = new AtomicBoolean(false);
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static ConnectionPool instance;

    /**
     * Array of open connections.
     */
    private ArrayBlockingQueue<ProxyConnection> connectionQueue;
    /**
     * Maximum number of open connections.
     */
    private final static AtomicInteger MAX_POOL_SIZE = new AtomicInteger(50);
    /**
     * Minimum number of open connections.
     */
    private final static AtomicInteger MIN_POOL_SIZE = new AtomicInteger(5);
    /**
     * Current number of open connections.
     */
    private static AtomicInteger poolSize = MIN_POOL_SIZE;

    /**
     * Returns a instance of connection pool.
     *
     * @param isTest If isTest true than connection pool opens connections to test database.
     * @return returns a instance of connection pool.
     */
    public static ConnectionPool getInstance(boolean isTest) {
        if (!instanceCreated.get()) {
            lock.lock();
            try {
                if (!instanceCreated.get()) {
                    try {
                        instance = new ConnectionPool(poolSize.get(), isTest);
                        Thread thread = new Thread(ConnectionPool::watchConnections);
                        thread.setDaemon(true);
                        thread.start();
                        Logger.log(Level.INFO, "Initialization connection pool. Start deamon thread to watching connection pool's size.");
                    } catch (SQLException e) {
                        Logger.log(Level.ERROR, e);
                    }
                    instanceCreated.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * @param poolSize number of connections.
     * @param isTest   If isTest true than connection pool opens connections to test database.
     * @throws SQLException
     */
    private ConnectionPool(final int poolSize, boolean isTest) throws SQLException {
        String dbName = ConnectionManager.getProperty(isTest ? "testDbName" : "dbName");

        BetDAO.setDbName(dbName);
        CategoryCoeffDAO.setDbName(dbName);
        CoefficientDAO.setDbName(dbName);
        MatchDAO.setDbName(dbName);
        MemberDAO.setDbName(dbName);
        UserDAO.setDbName(dbName);

        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

        connectionQueue = new ArrayBlockingQueue<>(MAX_POOL_SIZE.get());
        for (int i = 0; i < poolSize; i++) {
            ProxyConnection connection = new ProxyConnection(createConnection());
            connectionQueue.offer(connection);
        }
    }

    private Connection createConnection() throws SQLException {
        Properties properties = new Properties();
        properties.setProperty("user", ConnectionManager.getProperty("user"));
        properties.setProperty("password", ConnectionManager.getProperty("password"));

        String URL = ConnectionManager.getProperty("URL");


        return DriverManager.getConnection(URL, properties);
    }

    public ProxyConnection getConnection() throws InterruptedException {
        ProxyConnection connection = null;
        connection = connectionQueue.take();
        isActive.set(true);
        return connection;
    }

    public void closeConnection(ProxyConnection connection) {
        connectionQueue.offer(connection);
        isActive.set(true);
    }

    /**
     * A method that is run in a separate thread to change the number of open connections, depending on the busy connections.
     * If the free connections end, then the number of open connections is doubled.
     * If the number of busy connections is less than 1/2 of open connections,
     * then the number of open connections is reduced by half.
     */

    public static void watchConnections() {
        ConnectionPool connectionPool = getInstance(false);
        while (true) {
            if (isActive.get() && poolSize.get() < MAX_POOL_SIZE.get() && connectionPool.connectionQueue.size() == 0) {
                int overSize = poolSize.get() + poolSize.get() / 2 > MAX_POOL_SIZE.get() ? MAX_POOL_SIZE.get() - poolSize.get() : poolSize.get() / 2;
                poolSize.set(poolSize.get() + overSize);
                for (int i = 0; i < overSize; i++) {
                    ProxyConnection connection = null;
                    try {
                        connection = new ProxyConnection(connectionPool.createConnection());
                        connectionPool.connectionQueue.offer(connection);
                        isActive.set(false);
                    } catch (SQLException e) {
                        Logger.log(Level.ERROR, e);
                    }
                }
            } else if (isActive.get() && poolSize.get() > MIN_POOL_SIZE.get() && connectionPool.connectionQueue.size() > poolSize.get() / 2) {
                int overSize = connectionPool.connectionQueue.size() / 2;
                poolSize.set(poolSize.get() - overSize);
                for (int i = 0; i < overSize && poolSize.get() < MAX_POOL_SIZE.get(); i++) {
                    try {
                        connectionPool.connectionQueue.take().close();
                        isActive.set(false);
                    } catch (InterruptedException | SQLException e) {
                        Logger.log(Level.ERROR, e);
                    }
                }
            }
        }
    }
}
