package by.levchenko.database.connectionpool;

import java.util.ResourceBundle;

/**
 * The class retrieves information from the connectionPool.properties file.
 *
 * @author Vladislav Levchenko
 */

public class ConnectionManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("connectionPool");

    private ConnectionManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}