package by.levchenko.database.dao;

import by.levchenko.database.entity.User;
import by.levchenko.database.entity.UserRole;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to access database table 'user'.
 *
 * @author Vladislav Levchenko
 */

public class UserDAO extends AbstractDAO<User> {

    private Logger Logger = LogManager.getLogger(UserDAO.class);

    public final String SQL_SELECT_USER_BY_ID = "SELECT U_id, U_username, U_password, U_email, U_money, r.r_name FROM " + dbName + ".user u, " + dbName + ".role r where u.role_r_id = r.r_id && u_id = ?";

    public final String SQL_SELECT_USER_BY_USERNAME = "SELECT U_id, U_username, U_password, U_email, U_money, r.r_name FROM " + dbName + ".user u, " + dbName + ".role r where u.role_r_id = r.r_id && u_username = ?";
    public final String SQL_SELECT_ALL_USERS = "SELECT U_id, U_username, U_password, U_email, U_money, r.r_name FROM " + dbName + ".user u, " + dbName + ".role r where u.role_r_id = r.r_id";
    public final String SQL_INSERT_USER = "INSERT INTO " + dbName + ".user (U_username, U_password, U_email, U_money, Role_r_id) " +
            "VALUES(?,?,?,?, (select r_id from " + dbName + ".role r where r.r_name = ?))";
    public final String SQL_UPDATE_PASSWORD = "UPDATE " + dbName + ".user SET user.u_password = ? WHERE user.u_id = ?";
    public final String SQL_UPDATE_USER = "UPDATE " + dbName + ".user SET user.u_username = ?, user.u_password = ?, user.u_email = ?, user.u_money = ? WHERE user.u_id = ?";
    public final String SQL_DELETE_USER = "DELETE FROM " + dbName + ".user where u_id = ?";

    public UserDAO(Connection connection) {
        super(connection);
    }

    public static void setDbName(String newDbName) {
        dbName = newDbName;
    }

    public String getDbName() {
        return dbName;
    }

    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_USERS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = createUser(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return users;
    }

    @Override
    public User findEntityById(int id) {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createUser(resultSet);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    public User findEntityByUsername(String username) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_USERNAME)) {
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createUser(resultSet);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        return DAOUtil.executeUpdateById(connection, SQL_DELETE_USER, id);
    }

    @Override
    public boolean delete(User entity) {
        return delete(entity.getId());
    }

    @Override
    public boolean create(User user) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER)) {
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setBigDecimal(4, user.getBalance());
            preparedStatement.setString(5, user.getRole().toString());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return false; /////////////////
    }

    public boolean changePassword(User user, String new_password) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PASSWORD)) {
            preparedStatement.setString(1, new_password);
            preparedStatement.setInt(2, user.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return false; /////////////////
    }

    @Override
    public User update(User entity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER)) {
            preparedStatement.setString(1, entity.getUsername());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setString(3, entity.getEmail());
            preparedStatement.setBigDecimal(4, entity.getBalance());
            preparedStatement.setInt(5, entity.getId());
            preparedStatement.executeUpdate();
            return entity;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    private User createUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("u_id"));
        user.setUsername(resultSet.getString("u_username"));
        user.setPassword(resultSet.getString("u_password"));
        user.setBalance(resultSet.getBigDecimal("u_money"));
        user.setEmail(resultSet.getString("u_email"));
        user.setRole(UserRole.valueOf(resultSet.getString("r_name").toUpperCase()));
        return user;
    }
}

// реализации методов}
