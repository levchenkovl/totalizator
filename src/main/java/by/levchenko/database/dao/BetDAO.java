package by.levchenko.database.dao;

import by.levchenko.database.entity.Bet;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to access database table 'bet'.
 *
 * @author Vladislav Levchenko
 */

public class BetDAO extends AbstractDAO<Bet> {

    private static Logger Logger = LogManager.getLogger(BetDAO.class);

    public final String SQL_SELECT_BET_BY_ID = "SELECT b_id, b_value, b_gain, b_time_finished, coefficient_c_id, user_u_id FROM " + dbName + ".bet WHERE b_id = ?";
    public final String SQL_SELECT_BET_BY_USER_ID = "SELECT b_id, b_value, b_gain, b_time_finished, coefficient_c_id, user_u_id FROM " + dbName + ".bet WHERE  user_u_id = ?";
    public final String SQL_SELECT_ALL_BET = "SELECT b_id, b_value, b_gain, b_time_finished, coefficient_c_id, user_u_id FROM " + dbName + ".bet";
    public final String SQL_DELETE_BY_ID = "DELETE FROM " + dbName + ".bet where b_id = ?";
    public final String SQL_INSERT_BET = "INSERT INTO " + dbName + ".bet (b_value, coefficient_c_id, user_u_id)" +
            " VALUES(?,?,?)";
    public final String SQL_UPDATE_BET = "update " + dbName + ".bet set b_value = ?, b_gain=?, b_time_finished=?,coefficient_c_id=?, user_u_id=? where b_id = ?";

    public BetDAO(Connection connection) {
        super(connection);
    }

    public static void setDbName(String newDbName) {
        dbName = newDbName;
    }

    public static String getDbName() {
        return dbName;
    }

    @Override
    public List<Bet> findAll() {
        List<Bet> bets = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_BET)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Bet bet = createBet(resultSet);
                bets.add(bet);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return bets;
    }

    @Override
    public Bet findEntityById(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BET_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createBet(resultSet);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        return DAOUtil.executeUpdateById(connection, SQL_DELETE_BY_ID, id);
//        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Bet entity) {
//        return delete(entity.getId());
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(Bet entity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_BET)) {
            preparedStatement.setBigDecimal(1, entity.getValue());
            preparedStatement.setInt(2, entity.getCoefficientId());
            preparedStatement.setInt(3, entity.getUserId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return false;
    }

    @Override
    public Bet update(Bet entity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_BET)) {
            preparedStatement.setBigDecimal(1, entity.getValue());
            preparedStatement.setBigDecimal(2, entity.getGain());
            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(entity.getTime());
            preparedStatement.setString(3, date);
            preparedStatement.setInt(4, entity.getCoefficientId());
            preparedStatement.setInt(5, entity.getUserId());
            preparedStatement.setInt(6, entity.getId());
            preparedStatement.executeUpdate();
            return entity;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    private Bet createBet(ResultSet resultSet) throws SQLException {
        Bet bet = new Bet();
        bet.setId(resultSet.getInt("b_id"));
        bet.setTime(resultSet.getTimestamp("b_time_finished"));
        bet.setValue(resultSet.getBigDecimal("b_value"));
        bet.setGain(resultSet.getBigDecimal("b_gain"));
        bet.setCoefficientId(resultSet.getInt("coefficient_c_id"));
        bet.setUserId(resultSet.getInt("user_u_id"));
        return bet;
    }

    public List<Bet> findEntityByUserId(int userId) {
        List<Bet> bets = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BET_BY_USER_ID)) {
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Bet bet = createBet(resultSet);
                bets.add(bet);
            }

            return bets;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }

        return null;
    }
}
