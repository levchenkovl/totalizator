package by.levchenko.database.dao;

import by.levchenko.database.entity.Match;
import by.levchenko.database.entity.Member;
import by.levchenko.database.entity.User;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to access database table 'match'.
 *
 * @author Vladislav Levchenko
 */

public class MatchDAO extends AbstractDAO<Match> {

    private Logger Logger = LogManager.getLogger(MatchDAO.class);

    public final String SQL_SELECT_MATCH_BY_ID =
            "SELECT m_id, m_is_finished, m_owner_result, m_guest_result, s_name, user_u_id, member_m_id1, member_m_id2" +
                    " FROM " + dbName + ".`match`, " + dbName + ".`sport` where sport_s_id = s_id and m_id = ?";
    public final String SQL_SELECT_ALL_MATCH =
            "SELECT m_id, m_is_finished, m_owner_result, m_guest_result, s_name, user_u_id, member_m_id1, member_m_id2" +
                    " FROM " + dbName + ".`match`, " + dbName + ".`sport` where sport_s_id = s_id";
    public final String SQL_UPDATE_MATCH =
            "update " + dbName + ".match set m_is_finished = ?, m_owner_result = ?, m_guest_result = ?, sport_s_id = (select s_id from " + dbName + ".sport where s_name = ?)," +
                    " user_u_id = (select user.u_id from " + dbName + ".user where u_username = ?)," +
                    " member_m_id1 = (select member.m_id from " + dbName + ".member where m_name = ?)," +
                    " member_m_id2 = (select member.m_id from " + dbName + ".member where m_name = ?) where m_id = ?";
    public final String SQL_DELETE_MATCH_BY_ID =
            "delete from " + dbName + ".match where m_id = ?";
    public final String SQL_INSERT_MATCH =
            "insert into " + dbName + ".match (sport_s_id, user_u_id, member_m_id1, member_m_id2)" +
                    " value((select s_id from  " + dbName + ".sport where s_name = ?)," +
                    "(select u_id from " + dbName + ".user where u_username = ?)," +
                    "(select member.m_id from " + dbName + ".member where m_name = ?)," +
                    "(select member.m_id from " + dbName + ".member where m_name = ?))";
    public final String SQL_MAX_ID = "SELECT MAX(m_id) as maxid FROM " + dbName + ".match";

    public MatchDAO(Connection connection) {
        super(connection);
    }

    public static void setDbName(String newDbName) {
        dbName = newDbName;
    }

    public String getDbName() {
        return dbName;
    }

    public int defineMaxId() {
        final String maxId = "maxid";
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_MAX_ID)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int nextId = resultSet.getInt(maxId);
                return nextId;
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return 0;
    }

    @Override
    public List<Match> findAll() {
        List<Match> matchs = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_MATCH)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Match match = createMatch(resultSet);
                matchs.add(match);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return matchs;
    }

    @Override
    public Match findEntityById(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_MATCH_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return createMatch(resultSet);
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        return DAOUtil.executeUpdateById(connection, SQL_DELETE_MATCH_BY_ID, id);
    }

    @Override
    public boolean delete(Match entity) {
        return delete(entity.getId());
    }

    @Override
    public boolean create(Match entity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_MATCH)) {
            preparedStatement.setString(1, entity.getSportName());
            preparedStatement.setString(2, entity.getNameCreator());
            preparedStatement.setString(3, entity.getOwnerName());
            preparedStatement.setString(4, entity.getGuestName());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return false;
    }

    @Override
    public Match update(Match entity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_MATCH)) {
            preparedStatement.setBoolean(1, entity.isFinished());
            preparedStatement.setFloat(2, entity.getOwnerResult());
            preparedStatement.setFloat(3, entity.getGuestResult());
            preparedStatement.setString(4, entity.getSportName());
            preparedStatement.setString(5, entity.getNameCreator());
            preparedStatement.setString(6, entity.getOwnerName());
            preparedStatement.setString(7, entity.getGuestName());
            preparedStatement.setInt(8, entity.getId());
            preparedStatement.executeUpdate();
            return entity;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    private Match createMatch(ResultSet resultSet) throws SQLException {
        Match match = new Match();
        match.setId(resultSet.getInt("m_id"));
        match.setFinished(resultSet.getBoolean("m_is_finished"));
        match.setOwnerResult(resultSet.getInt("m_owner_result"));
        match.setGuestResult(resultSet.getInt("m_guest_result"));
        match.setSportName(resultSet.getString("s_name"));
        int member_m_id1 = (resultSet.getInt("member_m_id1"));
        int member_m_id2 = (resultSet.getInt("member_m_id2"));
        int user_u_id = (resultSet.getInt("user_u_id"));
        MemberDAO memberDAO = new MemberDAO(connection);
        UserDAO userDAO = new UserDAO(connection);
        Member owner = memberDAO.findEntityById(member_m_id1);
        Member guest = memberDAO.findEntityById(member_m_id2);
        User user = userDAO.findEntityById(user_u_id);
        match.setOwnerName(owner != null ? owner.getName() : null);
        match.setGuestName(guest != null ? guest.getName() : null);
        match.setNameCreator(user != null ? user.getUsername() : null);
        return match;
    }
}
