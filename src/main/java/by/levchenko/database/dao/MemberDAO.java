package by.levchenko.database.dao;

import by.levchenko.database.entity.Member;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to access database table 'member'.
 *
 * @author Vladislav Levchenko
 */

public class MemberDAO extends AbstractDAO<Member> {

    private Logger Logger = LogManager.getLogger(MemberDAO.class);

    public final String SQL_SELECT_MEMBER_BY_NAME = "SELECT m_id, m_name FROM " + dbName + ".member WHERE m_name = ?";
    public final String SQL_SELECT_MEMBER_BY_ID = "SELECT m_id, m_name FROM " + dbName + ".member WHERE m_id = ?";
    public final String SQL_SELECT_ALL_MEMBER = "SELECT m_id, m_name FROM " + dbName + ".member";
    public final String SQL_INSERT_MEMBER = "INSERT INTO " + dbName + ".member (m_name) value(?)";

    public MemberDAO(Connection connection) {
        super(connection);
    }

    public static void setDbName(String newDbName) {
        dbName = newDbName;
    }

    public String getDbName() {
        return dbName;
    }

    @Override
    public List<Member> findAll() {
        List<Member> members = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_MEMBER)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Member Member = createMember(resultSet);
                members.add(Member);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return members;
    }

    @Override
    public Member findEntityById(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_MEMBER_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createMember(resultSet);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    public Member findEntityByName(String name) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_MEMBER_BY_NAME)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createMember(resultSet);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Member entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(Member entity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_MEMBER)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return false;
    }

    @Override
    public Member update(Member entity) {
        throw new UnsupportedOperationException();
    }

    private Member createMember(ResultSet resultSet) throws SQLException {
        Member Member = new Member();
        Member.setId(resultSet.getInt("m_id"));
        Member.setName(resultSet.getString("m_name"));
        return Member;
    }
}
