package by.levchenko.database.dao;

import by.levchenko.database.entity.Coefficient;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to access database table 'coefficient'.
 *
 * @author Vladislav Levchenko
 */

public class CoefficientDAO extends AbstractDAO<Coefficient> {

    private Logger Logger = LogManager.getLogger(CoefficientDAO.class);

    public final String SQL_SELECT_COEFFICIENT_BY_ID =
            "SELECT c_id, c_value, c_exp_owner, c_exp_guest, c_is_available, u_username, match_m_id, cc_name FROM " + dbName + ".coefficient left join " + dbName + ".user on user_u_id = u_id, " + dbName + ".category_coeff  where cc_id = category_coeff_cc_id and c_id = ?";
    public final String SQL_SELECT_ALL_COEFFICIENT =
            "SELECT c_id, c_value, c_exp_owner, c_exp_guest, c_is_available, u_username, match_m_id, cc_name FROM " + dbName + ".coefficient left join " + dbName + ".user on user_u_id = u_id, " + dbName + ".category_coeff  where cc_id = category_coeff_cc_id";
    public final String SQL_UPDATE_COEFFICIENT =
            "update " + dbName + ".coefficient set c_value=?, c_exp_owner=?, c_exp_guest=?,  c_is_available = ?, user_u_id=(select u_id from " + dbName + ".user where u_username= ?), match_m_id=?, category_coeff_cc_id=(select cc_id from " + dbName + ".category_coeff where cc_name = ?) where c_id = ?";
    public final String SQL_INSERT_COEFFICIENT =
            "INSERT INTO `" + dbName + "`.`coefficient` (`c_value`, `c_exp_owner`, `c_exp_guest`, `c_is_available`, `user_u_id`, `match_m_id`, `category_coeff_cc_id`) VALUES (?, ?, ?, ?, (select u_id from  " + dbName + ".user where u_username = ?), ?, (select cc_id from " + dbName + ".category_coeff where cc_name = ?))";

    public CoefficientDAO(Connection connection) {
        super(connection);
    }

    public static void setDbName(String newDbName) {
        dbName = newDbName;
    }

    public String getDbName() {
        return dbName;
    }

    @Override
    public List<Coefficient> findAll() {
        List<Coefficient> coefficients = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_COEFFICIENT)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Coefficient coefficient = createCoefficient(resultSet);
                coefficients.add(coefficient);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return coefficients;
    }

    @Override
    public Coefficient findEntityById(int id) {
        return findEntityBy(id, SQL_SELECT_COEFFICIENT_BY_ID);

    }

    public Coefficient findEntityByUserId(int userId) {
        return findEntityBy(userId, SQL_SELECT_COEFFICIENT_BY_ID);
    }

    private Coefficient findEntityBy(int id, String sql_request) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql_request)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createCoefficient(resultSet);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(Coefficient entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(Coefficient entity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_COEFFICIENT)) {
            preparedStatement.setFloat(1, entity.getValue());
            if (entity.getExpOwner() != null) {
                preparedStatement.setInt(2, entity.getExpOwner());
            } else {
                preparedStatement.setNull(2, Types.INTEGER);
            }
            if (entity.getExpGuest() != null) {
                preparedStatement.setInt(3, entity.getExpGuest());
            } else {
                preparedStatement.setNull(3, Types.INTEGER);
            }
            preparedStatement.setBoolean(4, entity.isAvailable());
            preparedStatement.setString(5, entity.getNameCreator());
            preparedStatement.setInt(6, entity.getMatchId());
            preparedStatement.setString(7, entity.getCategoryCoeff());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }

        return false;
    }

    @Override
    public Coefficient update(Coefficient entity) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_COEFFICIENT)) {
            preparedStatement.setFloat(1, entity.getValue());
            preparedStatement.setFloat(2, entity.getExpOwner());
            preparedStatement.setFloat(3, entity.getExpGuest());
            preparedStatement.setBoolean(4, entity.isAvailable());
            preparedStatement.setString(5, entity.getNameCreator());
            preparedStatement.setInt(6, entity.getMatchId());
            preparedStatement.setString(7, entity.getCategoryCoeff());
            preparedStatement.setInt(8, entity.getId());
            preparedStatement.executeUpdate();
            return entity;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return null;
    }

    private Coefficient createCoefficient(ResultSet resultSet) throws SQLException {
        Coefficient coefficient = new Coefficient();
        coefficient.setId(resultSet.getInt("c_id"));
        coefficient.setValue(resultSet.getFloat("c_value"));
        coefficient.setExpOwner(resultSet.getInt("c_exp_owner"));
        coefficient.setExpGuest(resultSet.getInt("c_exp_guest"));
        coefficient.setAvailable(resultSet.getBoolean("c_is_available"));
        coefficient.setNameCreator(resultSet.getString("u_username"));
        coefficient.setMatchId(resultSet.getInt("match_m_id"));
        coefficient.setCategoryCoeff(resultSet.getString("cc_name"));
        return coefficient;
    }
}
