package by.levchenko.database.dao;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Methods for the logic.
 *
 * @author Vladislav Levchenko
 */

public class DAOUtil {

    private static Logger Logger = LogManager.getLogger(DAOUtil.class);

    public static boolean executeUpdateById(Connection connection, String sql, int id) {

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return false;
    }
}
