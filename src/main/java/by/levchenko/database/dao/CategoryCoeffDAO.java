package by.levchenko.database.dao;

import by.levchenko.database.entity.CategoryCoeff;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to access database table 'category_coeff'.
 *
 * @author Vladislav Levchenko
 */

public class CategoryCoeffDAO extends AbstractDAO<CategoryCoeff> {

    private Logger Logger = LogManager.getLogger(CategoryCoeffDAO.class);

    public final String SQL_SELECT_CATEGORY_COEFF_BY_ID = "SELECT cc_id, cc_name FROM " + dbName + ".category_coeff WHERE r_id = ?";
    public final String SQL_SELECT_ALL_CATEGORY_COEFF = "SELECT cc_id, cc_name FROM " + dbName + ".category_coeff";

    public CategoryCoeffDAO(Connection connection) {
        super(connection);
    }

    public static void setDbName(String newDbName) {
        dbName = newDbName;
    }

    public String getDbName() {
        return dbName;
    }

    @Override
    public List<CategoryCoeff> findAll() {
        List<CategoryCoeff> categoryCoeffs = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_CATEGORY_COEFF)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CategoryCoeff categoryCoeff = createCategoryBet(resultSet);
                categoryCoeffs.add(categoryCoeff);
            }
        } catch (SQLException e) {
            Logger.log(Level.ERROR, e);
        }
        return categoryCoeffs;
    }

    @Override
    public CategoryCoeff findEntityById(int id) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_CATEGORY_COEFF_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return createCategoryBet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(CategoryCoeff entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean create(CategoryCoeff entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CategoryCoeff update(CategoryCoeff entity) {
        throw new UnsupportedOperationException();
    }

    private CategoryCoeff createCategoryBet(ResultSet resultSet) throws SQLException {
        CategoryCoeff categoryCoeff = new CategoryCoeff();
        categoryCoeff.setId(resultSet.getInt("cc_id"));
        categoryCoeff.setName(resultSet.getString("cc_name"));
        return categoryCoeff;
    }
}
