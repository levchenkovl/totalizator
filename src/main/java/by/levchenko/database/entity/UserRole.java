package by.levchenko.database.entity;

public enum UserRole {
    GUEST, CLIENT, BOOKMAKER, ADMINISTRATOR
}