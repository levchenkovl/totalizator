package by.levchenko.database.entity;

import java.io.Serializable;

/**
 * An entity for storing information from a database's table 'match'.
 *
 * @author Vladislav Levchenko
 */

public class Match extends Entity implements Serializable, Cloneable {

    /**
     * If true match is finished. Known result of the match.
     */
    private boolean isFinished;

    /**
     * First member's result.
     */
    private int ownerResult;

    /**
     * Second member's result.
     */
    private int guestResult;

    /**
     * Name of administrator who created the match.
     */
    private String nameCreator;

    /**
     * Kind of sport.
     */
    private String sportName;

    /**
     * First member's name.
     */
    private String ownerName;

    /**
     * Second member's name.
     */
    private String guestName;

    public Match() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public int getOwnerResult() {
        return ownerResult;
    }

    public void setOwnerResult(int ownerResult) {
        this.ownerResult = ownerResult;
    }

    public int getGuestResult() {
        return guestResult;
    }

    public void setGuestResult(int guestResult) {
        this.guestResult = guestResult;
    }

    public String getNameCreator() {
        return nameCreator;
    }

    public void setNameCreator(String nameCreator) {
        this.nameCreator = nameCreator;
    }

    public String getSportName() {
        return sportName;
    }

    public void setSportName(String sportName) {
        this.sportName = sportName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Match match = (Match) o;

        if (id != match.id) return false;
        if (isFinished != match.isFinished) return false;
        if (ownerResult != match.ownerResult) return false;
        if (guestResult != match.guestResult) return false;
        if (nameCreator != null ? !nameCreator.equals(match.nameCreator) : match.nameCreator != null) return false;
        if (sportName != null ? !sportName.equals(match.sportName) : match.sportName != null) return false;
        if (ownerName != null ? !ownerName.equals(match.ownerName) : match.ownerName != null) return false;
        return guestName != null ? guestName.equals(match.guestName) : match.guestName == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (isFinished ? 1 : 0);
        result = 31 * result + ownerResult;
        result = 31 * result + guestResult;
        result = 31 * result + (nameCreator != null ? nameCreator.hashCode() : 0);
        result = 31 * result + (sportName != null ? sportName.hashCode() : 0);
        result = 31 * result + (ownerName != null ? ownerName.hashCode() : 0);
        result = 31 * result + (guestName != null ? guestName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", isFinished=" + isFinished +
                ", ownerResult=" + ownerResult +
                ", guestResult=" + guestResult +
                ", nameCreator='" + nameCreator + '\'' +
                ", sportName='" + sportName + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", guestName='" + guestName + '\'' +
                '}';
    }
}
