package by.levchenko.database.entity;

import java.io.Serializable;

/**
 * An entity for storing information from a database's table 'coefficient'.
 *
 * @author Vladislav Levchenko
 */

public class Coefficient extends Entity implements Serializable, Cloneable {

    /**
     * Coefficient value.
     */
    private float value;

    /**
     * If coefficient's type is 'result'. Field contains expected result of first member.
     */
    private Integer expOwner;

    /**
     * If coefficient's type is 'result'. Field contains expected result of second member.
     */
    private Integer expGuest;

    /**
     * If true the client can bet on this coefficient.
     * If false the client can no longer bet on this ratio.
     */
    private boolean isAvailable;

    /**
     * Name of the coefficient's creator.
     */
    private String nameCreator;

    /**
     * The number of the match for which the coefficient is set.
     */
    private int matchId;

    /**
     * Kind of coefficient.
     */
    private String categoryCoeff;

    public Coefficient() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public Integer getExpOwner() {
        return expOwner;
    }

    public void setExpOwner(Integer expOwner) {
        this.expOwner = expOwner;
    }

    public Integer getExpGuest() {
        return expGuest;
    }

    public void setExpGuest(Integer expGuest) {
        this.expGuest = expGuest;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getNameCreator() {
        return nameCreator;
    }

    public void setNameCreator(String nameCreator) {
        this.nameCreator = nameCreator;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public String getCategoryCoeff() {
        return categoryCoeff;
    }

    public void setCategoryCoeff(String categoryCoeff) {
        this.categoryCoeff = categoryCoeff;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coefficient that = (Coefficient) o;

        if (Float.compare(that.value, value) != 0) return false;
        if (isAvailable != that.isAvailable) return false;
        if (matchId != that.matchId) return false;
        if (expOwner != null ? !expOwner.equals(that.expOwner) : that.expOwner != null) return false;
        if (expGuest != null ? !expGuest.equals(that.expGuest) : that.expGuest != null) return false;
        if (nameCreator != null ? !nameCreator.equals(that.nameCreator) : that.nameCreator != null) return false;
        return categoryCoeff != null ? categoryCoeff.equals(that.categoryCoeff) : that.categoryCoeff == null;
    }

    @Override
    public int hashCode() {
        int result = (value != +0.0f ? Float.floatToIntBits(value) : 0);
        result = 31 * result + (expOwner != null ? expOwner.hashCode() : 0);
        result = 31 * result + (expGuest != null ? expGuest.hashCode() : 0);
        result = 31 * result + (isAvailable ? 1 : 0);
        result = 31 * result + (nameCreator != null ? nameCreator.hashCode() : 0);
        result = 31 * result + matchId;
        result = 31 * result + (categoryCoeff != null ? categoryCoeff.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Coefficient{" +
                "value=" + value +
                ", expOwner=" + expOwner +
                ", expGuest=" + expGuest +
                ", isAvailable=" + isAvailable +
                ", nameCreator='" + nameCreator + '\'' +
                ", matchId=" + matchId +
                ", categoryCoeff='" + categoryCoeff + '\'' +
                ", id=" + id +
                '}';
    }
}
