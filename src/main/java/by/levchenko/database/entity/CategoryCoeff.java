package by.levchenko.database.entity;

import java.io.Serializable;

/**
 * An entity for storing information from a database's table 'category_coeff'.
 *
 * @author Vladislav Levchenko
 */

public class CategoryCoeff extends Entity implements Serializable, Cloneable {

    /**
     * Kind of coefficient.
     */
    private String name;

    public CategoryCoeff() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryCoeff that = (CategoryCoeff) o;

        if (id != that.id) return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CategoryCoeff{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
