package by.levchenko.database.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * An entity for storing information from a database's table 'bet'.
 *
 * @author Vladislav Levchenko
 */

public class Bet extends Entity implements Serializable, Cloneable {

    /**
     * The time at which the bet ends. Known result of the match.
     */
    private Date time;

    /**
     * Bet amount.
     */
    private BigDecimal value;

    /**
     * Winning amount.
     */
    private BigDecimal gain;

    /**
     * If isWin is true the bet won.
     */
    private boolean isWin;

    /**
     * Coefficient id on which the bet is made
     */
    private int coefficientId;

    /**
     * User id that made a bet
     */
    private int userId;

    public Bet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getGain() {
        return gain;
    }

    public void setGain(BigDecimal gain) {
        this.gain = gain;
    }

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean win) {
        isWin = win;
    }

    public int getCoefficientId() {
        return coefficientId;
    }

    public void setCoefficientId(int coefficientId) {
        this.coefficientId = coefficientId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bet bet = (Bet) o;

        if (id != bet.id) return false;
        if (isWin != bet.isWin) return false;
        if (coefficientId != bet.coefficientId) return false;
        if (userId != bet.userId) return false;
        if (time != null ? !time.equals(bet.time) : bet.time != null) return false;
        if (value != null ? !value.equals(bet.value) : bet.value != null) return false;
        return gain != null ? gain.equals(bet.gain) : bet.gain == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (gain != null ? gain.hashCode() : 0);
        result = 31 * result + (isWin ? 1 : 0);
        result = 31 * result + coefficientId;
        result = 31 * result + userId;
        return result;
    }

    @Override
    public String toString() {
        return "Bet{" +
                "id=" + id +
                ", time=" + time +
                ", value=" + value +
                ", gain=" + gain +
                ", isWin=" + isWin +
                ", coefficientId=" + coefficientId +
                ", userId=" + userId +
                '}';
    }
}
