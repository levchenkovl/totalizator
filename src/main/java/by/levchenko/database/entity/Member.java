package by.levchenko.database.entity;

import java.io.Serializable;

/**
 * An entity for storing information from a database's table 'member'.
 *
 * @author Vladislav Levchenko
 */

public class Member extends Entity implements Serializable, Cloneable {

    /**
     * Member's name.
     */
    private String name;

    public Member() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Member member = (Member) o;

        if (id != member.id) return false;
        return name != null ? name.equals(member.name) : member.name == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
