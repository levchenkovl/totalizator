package by.levchenko.command;


import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.page.EmptyCommand;
import by.levchenko.command.utility.CommandEnum;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {

    /**
     * @param request request received by the controller.
     * @return command to be performed.
     */
    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action
                    + MessageManager.getProperty("message.wrongaction"));
        }
        return current;
    }
}