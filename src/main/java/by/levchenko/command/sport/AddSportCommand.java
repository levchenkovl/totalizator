package by.levchenko.command.sport;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.database.entity.User;
import by.levchenko.logic.action.SportAction;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.SportException;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;


/**
 * A command that is executed when the administrator creates the match.
 *
 * @author Vladislav Levchenko
 */

public class AddSportCommand implements ActionCommand {


    private static Logger Logger = LogManager.getLogger(AddSportCommand.class);
    private static final String PATTERN_MEMBERNAME = "^(?=.{1,20}$)(?![_.])(?!.*[_.]{2})[\\w\\d._]+(?<![_.])$";

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.SPORT.getPage());


        try {
            request.setCharacterEncoding("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        User user = (User) request.getSession().getAttribute("user");

        String nameSport = request.getParameter("name_sport");
        String owner = request.getParameter("owner");
        String guest = request.getParameter("guest");

        try (TransactionManager transactionManager = new TransactionManager()) {
            UserAction userAction = new UserAction(transactionManager.getConnection());
            if (!userAction.checkWithRegexp(owner, PATTERN_MEMBERNAME) || !userAction.checkWithRegexp(guest, PATTERN_MEMBERNAME)) {
                request.setAttribute("status",
                        MessageManager.getProperty("message.incorrectData"));
                page = ConfigurationManager.getProperty(PageEnum.SPORT.getPage());
                return page;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("status", "fail");

        if (nameSport == null || owner == null || guest == null) {
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
            return page;
        }

        try (TransactionManager transactionManager = new TransactionManager()) {
            transactionManager.setAutoCommit(false);

            SportAction sportAction = new SportAction(transactionManager.getConnection());
            if (sportAction.addSport(user.getUsername(), nameSport, owner, guest)) {
                transactionManager.commit();

                request.setAttribute("status", MessageManager.getProperty("message.success"));
                new PrintSportsCommand().execute(request);
            }
        } catch (SportException e) {
            request.setAttribute("status", MessageManager.getProperty("message.cantAddSport"));
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }


        return page;
    }
}
