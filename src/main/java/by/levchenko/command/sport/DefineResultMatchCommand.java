package by.levchenko.command.sport;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.logic.action.SportAction;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the administrator defines the result of the match.
 *
 * @author Vladislav Levchenko
 */

public class DefineResultMatchCommand implements ActionCommand {


    private static Logger Logger = LogManager.getLogger(DefineResultMatchCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.SPORT.getPage());

        String matchIdParam = request.getParameter("matchId");

        if (matchIdParam == null) {
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
            return page;
        }

        int matchId = 0;
        try {
            matchId = Integer.parseInt(matchIdParam);
        } catch (NumberFormatException e) {
            Logger.log(Level.ERROR, e);
        }

        try (TransactionManager transactionManager = new TransactionManager()) {
            transactionManager.setAutoCommit(false);

            SportAction sportAction = new SportAction(transactionManager.getConnection());
            if (matchId != 0) {
                sportAction.defineResultMatch(matchId);
                transactionManager.commit();

                request.setAttribute("status", MessageManager.getProperty("message.success"));
            }
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        new PrintSportsCommand().execute(request);

        return page;
    }
}
