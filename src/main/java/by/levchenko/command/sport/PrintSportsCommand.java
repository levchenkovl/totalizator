package by.levchenko.command.sport;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.database.entity.CategoryCoeff;
import by.levchenko.logic.action.CoeffAction;
import by.levchenko.logic.action.SportAction;
import by.levchenko.logic.entity.SportForJSP;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * A command that is executed when the user wants to see the page with list of the match.
 *
 * @author Vladislav Levchenko
 */

public class PrintSportsCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(PrintSportsCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.HOME.getPage());

        try (TransactionManager transactionManager = new TransactionManager()) {
            transactionManager.setAutoCommit(false);

            CoeffAction coeffAction = new CoeffAction(transactionManager.getConnection());
            SportAction sportAction = new SportAction(transactionManager.getConnection());
            List<SportForJSP> sportForJSPS = sportAction.findAll(false);
            List<CategoryCoeff> categoryCoeffs = coeffAction.findAllCategoryCoeff();

            request.getSession().setAttribute("coefficients", sportForJSPS);
            request.getSession().setAttribute("categoryCoeffs", categoryCoeffs);
            page = ConfigurationManager.getProperty(PageEnum.SPORT.getPage());
            transactionManager.commit();
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        return page;
    }
}
