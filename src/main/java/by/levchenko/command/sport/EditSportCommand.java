package by.levchenko.command.sport;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.command.utility.UtilCommand;
import by.levchenko.database.entity.User;
import by.levchenko.logic.action.SportAction;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the administrator edits the match.
 *
 * @author Vladislav Levchenko
 */

public class EditSportCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(EditSportCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = UtilCommand.definePage(request);

        User user = (User) request.getSession().getAttribute("user");

        String matchIdParam = request.getParameter("matchId");
        String sportNameParam = request.getParameter("sportName");
        String ownerParam = request.getParameter("owner");
        String guestParam = request.getParameter("guest");

        if (page == null || matchIdParam == null || sportNameParam == null || ownerParam == null || guestParam == null) {
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
            return page;
        }

        request.setAttribute("status", "Fail!");

        int matchId = 0;
        String sportName = "";
        String owner = "";
        String guest = "";
        try (TransactionManager transactionManager = new TransactionManager()) {
            transactionManager.setAutoCommit(false);

            SportAction sportAction = new SportAction(transactionManager.getConnection());
            matchId = Integer.parseInt(matchIdParam);
            sportName = sportNameParam;
            owner = ownerParam;
            guest = guestParam;

            if (sportAction.updateMatchById(matchId, sportName, owner, guest, user.getUsername())) {
                transactionManager.commit();

                request.setAttribute("status", MessageManager.getProperty("message.success"));
                new PrintSportsCommand().execute(request);
            }
        } catch (NumberFormatException e) {
            request.setAttribute("status", MessageManager.getProperty("message.badMatchID"));
            Logger.log(Level.ERROR, e);
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }


        return page;
    }
}
