package by.levchenko.command.manager;

import java.util.ResourceBundle;

/**
 * The class retrieves information from the config.properties file.
 *
 * @author Vladislav Levchenko
 */

public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    private ConfigurationManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}