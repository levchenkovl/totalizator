package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.utility.PageEnum;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the user wats to logout.
 *
 * @author Vladislav Levchenko
 */

public class LogoutCommand implements ActionCommand {

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());

        final String PARAM_LOCALE = "locale";

        String locale = (String) request.getSession().getAttribute(PARAM_LOCALE);
        request.getSession().invalidate();
        request.getSession().setAttribute(PARAM_LOCALE, locale);
        return page;
    }
}