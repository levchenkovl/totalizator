package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.database.entity.User;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.UserException;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed to send the link to register.
 *
 * @author Vladislav Levchenko
 */

public class SendRegisterCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(SendRegisterCommand.class);

    private static final String PATTERN_PASSWORD = "^(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*[\\d])\\S{8,}$";
    private static final String PATTERN_USERNAME = "^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[\\w\\d._]+(?<![_.])$";
    private static final String PATTERN_EMAIL = "^[a-zA-Z0-9.!#$%&’*+\\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");

        String page = ConfigurationManager.getProperty(user != null ? PageEnum.USERS.getPage() : PageEnum.LOGIN.getPage());

        String login = request.getParameter("username");
        String pass = request.getParameter("password");
        String email = request.getParameter("email");
        String role = request.getParameter("role");

        if (login == null || pass == null || email == null) {
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
            return page;
        }
        try (TransactionManager transactionManager = new TransactionManager()) {
            UserAction userAction = new UserAction(transactionManager.getConnection());
            if (!userAction.checkWithRegexp(pass, PATTERN_PASSWORD) || !userAction.checkWithRegexp(login, PATTERN_USERNAME)
                    || !userAction.checkWithRegexp(email, PATTERN_EMAIL)) {
                request.setAttribute("status",
                        MessageManager.getProperty("message.incorrectData"));
                page = ConfigurationManager.getProperty(user != null ? PageEnum.USERS.getPage() : PageEnum.REGISTER.getPage());
                return page;
            }

            if (userAction.loginAndEmailIsExist(login, email)) {
                request.setAttribute("status",
                        MessageManager.getProperty("message.loginOrEmailIsBusy"));
                page = ConfigurationManager.getProperty(user != null ? PageEnum.USERS.getPage() : PageEnum.REGISTER.getPage());
                return page;
            }

            if (userAction.sendRegister(login, pass, email, role)) {
                request.setAttribute("status", MessageManager.getProperty("message.confirmRegistration"));
            } else {
                request.setAttribute("status",
                        MessageManager.getProperty("message.registerError"));
                page = ConfigurationManager.getProperty(user != null ? PageEnum.USERS.getPage() : PageEnum.REGISTER.getPage());
                return page;
            }
        } catch (UserException e) {
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            Logger.log(Level.ERROR, e);
        }
        return page;
    }
}
