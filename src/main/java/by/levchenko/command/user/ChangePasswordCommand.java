package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.database.entity.User;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.UserException;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the user changes the password.
 *
 * @author Vladislav Levchenko
 */

public class ChangePasswordCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(ChangePasswordCommand.class);

    private static final String PATTERN_PASSWORD = "^(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*[\\d])\\S{8,}$";

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.USER_ACCOUNT.getPage());

        String current_password = request.getParameter("current_password");
        String new_password = request.getParameter("new_password");
        String again_password = request.getParameter("again_password");

        User user = (User) request.getSession().getAttribute("user");

        if (current_password == null || new_password == null || again_password == null) {
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
            return page;
        }

        if (!new_password.equals(again_password)) {
            request.setAttribute("status", MessageManager.getProperty("message.passwordsDontMatch"));
            return page;
        }


        try (TransactionManager transactionManager = new TransactionManager()) {
            UserAction userAction = new UserAction(transactionManager.getConnection());
            if (!userAction.checkPassword(current_password, user.getPassword())) {
                request.setAttribute("status", MessageManager.getProperty("message.incorrectPassword"));
                return page;
            }
            if (!userAction.checkWithRegexp(new_password, PATTERN_PASSWORD)) {
                request.setAttribute("status",
                        MessageManager.getProperty("message.incorrectPasswordRegex"));
                return page;
            }

            transactionManager.setAutoCommit(false);

            if (userAction.changePassword(user, new_password)) {
                user.setPassword(new_password);

                transactionManager.commit();

                request.setAttribute("status", MessageManager.getProperty("message.success"));
            } else {
                request.setAttribute("status", MessageManager.getProperty("message.fail"));
            }
        } catch (UserException e) {
            request.setAttribute("status", MessageManager.getProperty("message.cantChangePassword"));
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        return page;
    }
}
