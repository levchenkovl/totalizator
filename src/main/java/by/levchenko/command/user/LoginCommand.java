package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.database.entity.User;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.UserException;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the user wats to login.
 *
 * @author Vladislav Levchenko
 */

public class LoginCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(LoginCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter("username");
        String pass = request.getParameter("password");

        if (login == null || pass == null) {
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
            return page;
        }

        page = ConfigurationManager.getProperty(PageEnum.LOGIN.getPage());

        try (TransactionManager transactionManager = new TransactionManager()) {
            UserAction userAction = new UserAction(transactionManager.getConnection());
            User user = userAction.checkLoginAndPass(login, pass);
            request.getSession().setAttribute("user", user);
            request.getSession().setAttribute("userRole", user.getRole());
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
        } catch (UserException e) {
            request.setAttribute("status",
                    MessageManager.getProperty("message.loginError"));
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            request.setAttribute("status",
                    MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status",
                    MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }
        return page;
    }
}