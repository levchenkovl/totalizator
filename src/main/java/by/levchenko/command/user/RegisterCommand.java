package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.command.utility.UtilCommand;
import by.levchenko.database.entity.UserRole;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.UserException;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed to register user after confirmation of registration at the mail.
 *
 * @author Vladislav Levchenko
 */

public class RegisterCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(RegisterCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.LOGIN.getPage());

        UserRole userRole = UtilCommand.defineUserRole(request);
        String newUser = request.getParameter("newUser");
        newUser = newUser.replaceAll(" ", "+");


        try (TransactionManager transactionManager = new TransactionManager()) {
            UserAction userAction = new UserAction(transactionManager.getConnection());
            if (userAction.register(newUser)) {
                request.setAttribute("status", MessageManager.getProperty("message.successRegister"));
            } else {
                request.setAttribute("status",
                        MessageManager.getProperty("message.registerError"));
                page = ConfigurationManager.getProperty(PageEnum.REGISTER.getPage());
            }
        } catch (UserException e) {
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            Logger.log(Level.ERROR, e);
        }


        if (userRole.ordinal() == 3) {
            page = ConfigurationManager.getProperty(PageEnum.USERS.getPage());
            new PrintUsersCommand().execute(request);
        }
        return page;
    }
}
