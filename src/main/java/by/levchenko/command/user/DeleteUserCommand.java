package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.database.entity.User;
import by.levchenko.database.entity.UserRole;
import by.levchenko.logic.action.UserAction;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * A command that is executed when the administrator deletes user.
 *
 * @author Vladislav Levchenko
 */

public class DeleteUserCommand implements ActionCommand {


    private static Logger Logger = LogManager.getLogger(DeleteUserCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {

        String page = ConfigurationManager.getProperty(PageEnum.USERS.getPage());

        User user = (User) request.getSession().getAttribute("user");

        String username = request.getParameter("username");

        if (username == null || request.getParameter("userId") == null) {
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
            return page;
        }

        int userId = Integer.parseInt(request.getParameter("userId"));
        if (userId == user.getId()) {
            request.setAttribute("status", MessageManager.getProperty("message.cantDeleteYourself"));
            return page;
        }

        UserRole role = UserRole.valueOf(request.getParameter("role"));

        try (TransactionManager transactionManager = new TransactionManager()) {
            transactionManager.setAutoCommit(false);

            UserAction userAction = new UserAction(transactionManager.getConnection());
            if (userAction.deleteUser(userId, role)) {
                transactionManager.commit();

                new PrintUsersCommand().execute(request);
                request.setAttribute("status", MessageManager.getProperty("message.success"));
            } else {
                request.setAttribute("status", MessageManager.getProperty("message.fail"));
            }
            page = ConfigurationManager.getProperty(PageEnum.USERS.getPage());
            ArrayList<User> users = userAction.findAll();
            request.getSession().setAttribute("users", users);
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        return page;
    }
}
