package by.levchenko.command.user;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.command.utility.UtilCommand;
import by.levchenko.database.entity.User;
import by.levchenko.database.entity.UserRole;
import by.levchenko.logic.action.BetAction;
import by.levchenko.logic.action.CoeffAction;
import by.levchenko.logic.action.SportAction;
import by.levchenko.logic.entity.BetForJSP;
import by.levchenko.logic.entity.SportForJSP;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * A command that is executed when the user wants to see user's information.
 *
 * @author Vladislav Levchenko
 */

public class UserAccountCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(PrintUsersCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.USER_ACCOUNT.getPage());

        UserRole userRole = UtilCommand.defineUserRole(request);
        User user = (User) request.getSession().getAttribute("user");

        try (TransactionManager transactionManager = new TransactionManager()) {
            CoeffAction coeffAction = new CoeffAction(transactionManager.getConnection());
            SportAction sportAction = new SportAction(transactionManager.getConnection());
            BetAction betAction = new BetAction(transactionManager.getConnection());

            switch (userRole.ordinal()) {
                case 3:
                    ArrayList<SportForJSP> sports =
                            (ArrayList<SportForJSP>) sportAction.findSportsByUserId(user.getId());
                    request.getSession().setAttribute("coefficients", sports);
                    break;
                case 2:
                    ArrayList<SportForJSP> coefficients =
                            (ArrayList<SportForJSP>) coeffAction.findCoefficientsByUserId(user.getId());
                    request.getSession().setAttribute("coefficients", coefficients);
                    break;
                case 1:
                    ArrayList<BetForJSP> bets =
                            (ArrayList<BetForJSP>) betAction.findBetForJSPByUserId(user.getId());
                    request.getSession().setAttribute("bets", bets);
                    break;
            }
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
        }

        request.getSession().setAttribute("subUser", request.getSession().getAttribute("user"));

        return page;
    }
}
