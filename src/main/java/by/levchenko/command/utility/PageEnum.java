package by.levchenko.command.utility;

public enum PageEnum {
    INDEX {
        {
            this.page = "path.page.index";
        }
    },
    HOME {
        {
            this.page = "path.page.home";
        }
    },
    LOGIN {
        {
            this.page = "path.page.login";
        }
    },
    REGISTER {
        {
            this.page = "path.page.register";
        }
    },
    ERROR {
        {
            this.page = "path.page.error";
        }
    },
    USER_ACCOUNT {
        {
            this.page = "path.page.userAccount";
        }
    },
    SPORT {
        {
            this.page = "path.page.sport";
        }
    },
    USERS {
        {
            this.page = "path.page.users";
        }
    };
    String page;

    public String getPage() {
        return page;
    }
}
