package by.levchenko.command.utility;


import by.levchenko.database.entity.User;
import by.levchenko.database.entity.UserRole;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Methods for the commands.
 *
 * @author Vladislav Levchenko
 */

public class UtilCommand {

    private static final String USER = "user";

    /**
     * @param request received by some command.
     * @return the user's role.
     */
    public static UserRole defineUserRole(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute(USER);
        return user != null ? user.getRole() : UserRole.GUEST;
    }

    /**
     * @param request request received by some command.
     * @return the page from which the controller was called.
     */
    public static String definePage(HttpServletRequest request) {
        final String REGEXP_REFERERUS = "[^\\/^:]\\/.*";
        String page = request.getParameter("page");
        Pattern pattern = Pattern.compile(REGEXP_REFERERUS);
        Matcher matcher = pattern.matcher(page);
        if (matcher.find()) {
            page = matcher.group().substring(1);
        }
        return page;
    }
}
