package by.levchenko.command.utility;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.bet.AddBetCommand;
import by.levchenko.command.coeff.EditCoeffCommand;
import by.levchenko.command.page.ChangeLocaleCommand;
import by.levchenko.command.page.DefinePageCommand;
import by.levchenko.command.sport.*;
import by.levchenko.command.user.*;

public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    REGISTER {
        {
            this.command = new RegisterCommand();
        }
    },
    CHANGELANG {
        {
            this.command = new ChangeLocaleCommand();
        }
    },
    PRINT_USERS {
        {
            this.command = new PrintUsersCommand();
        }
    },
    CHANGE_PASSWORD {
        {
            this.command = new ChangePasswordCommand();
        }
    },
    PRINT_SPORTS {
        {
            this.command = new PrintSportsCommand();
        }
    },
    DELETE_USER {
        {
            this.command = new DeleteUserCommand();
        }
    },
    EDIT_USER {
        {
            this.command = new EditUserCommand();
        }
    },
    DELETE_SPORT {
        {
            this.command = new DeleteSportCommand();
        }
    },
    EDIT_SPORT {
        {
            this.command = new EditSportCommand();
        }
    },
    USER_ACCOUNT {
        {
            this.command = new UserAccountCommand();
        }
    },
    EDIT_COEFF {
        {
            this.command = new EditCoeffCommand();
        }
    },
    ADD_BET {
        {
            this.command = new AddBetCommand();
        }
    },
    RESULT_MATCH {
        {
            this.command = new DefineResultMatchCommand();
        }
    },
    ADD_SPORT {
        {
            this.command = new AddSportCommand();
        }
    },
    SEND_REGISTER {
        {
            this.command = new SendRegisterCommand();
        }
    },
    DEFINE_PAGE {
        {
            this.command = new DefinePageCommand();
        }
    };
    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}