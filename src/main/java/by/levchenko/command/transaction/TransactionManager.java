package by.levchenko.command.transaction;

import by.levchenko.database.connectionpool.ConnectionPool;
import by.levchenko.database.connectionpool.ProxyConnection;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.sql.SQLException;

/**
 * The class manages transactions of connection.
 *
 * @author Vladislav Levchenko
 */

public class TransactionManager implements AutoCloseable {

    private static Logger Logger = LogManager.getLogger(TransactionManager.class);

    /**
     * Instance of connection pool.
     */
    protected ConnectionPool connectionPool;

    /**
     * Connection for accessing the database.
     */
    private ProxyConnection connection;

    /**
     * If true, TransactionManager will close connection with commit or rollback.
     */
    private boolean isAutoCommit = true;

    /**
     * If true, TransactionManager will close connection with commit.
     * If false, TransactionManager will close connection with rollback.
     */
    private boolean isCommit;

    public TransactionManager() throws InterruptedException {
        connectionPool = ConnectionPool.getInstance(false);
        connection = connectionPool.getConnection();
        Logger.log(Level.DEBUG, "Init TransactionManager. Get connection from connection pool.");
    }

    public ProxyConnection getConnection() {
        return connection;
    }

    public void setConnection(ProxyConnection connection) {
        this.connection = connection;
    }

    public void setAutoCommit(boolean isAutoCommit) throws SQLException {
        connection.setAutoCommit(isAutoCommit);
        this.isAutoCommit = isAutoCommit;
        this.isCommit = false;
    }

    public void commit() throws SQLException {
        isCommit = true;
    }

    /**
     * Closes this resource, relinquishing any underlying resources.
     * This method is invoked automatically on objects managed by the
     * {@code try}-with-resources statement.
     * <p>
     * <p>While this interface method is declared to throw {@code
     * Exception}, implementers are <em>strongly</em> encouraged to
     * declare concrete implementations of the {@code close} method to
     * throw more specific exceptions, or to throw no exception at all
     * if the close operation cannot fail.
     * <p>
     * <p> Cases where the close operation may fail require careful
     * attention by implementers. It is strongly advised to relinquish
     * the underlying resources and to internally <em>mark</em> the
     * resource as closed, prior to throwing the exception. The {@code
     * close} method is unlikely to be invoked more than once and so
     * this ensures that the resources are released in a timely manner.
     * Furthermore it reduces problems that could arise when the resource
     * wraps, or is wrapped, by another resource.
     * <p>
     * <p><em>Implementers of this interface are also strongly advised
     * to not have the {@code close} method throw {@link
     * InterruptedException}.</em>
     * <p>
     * This exception interacts with a thread's interrupted status,
     * and runtime misbehavior is likely to occur if an {@code
     * InterruptedException} is {@linkplain Throwable#addSuppressed
     * suppressed}.
     * <p>
     * More generally, if it would cause problems for an
     * exception to be suppressed, the {@code AutoCloseable.close}
     * method should not throw it.
     * <p>
     * <p>Note that unlike the {@link Closeable#close close}
     * method of {@link Closeable}, this {@code close} method
     * is <em>not</em> required to be idempotent.  In other words,
     * calling this {@code close} method more than once may have some
     * visible side effect, unlike {@code Closeable.close} which is
     * required to have no effect if called more than once.
     * <p>
     * However, implementers of this interface are strongly encouraged
     * to make their {@code close} methods idempotent.
     *
     * @throws Exception if this resource cannot be closed
     */
    @Override
    public void close() throws Exception {
        if (!isAutoCommit) {
            if (isCommit) {
                connection.commit();
            } else {
                connection.rollback();
            }
        }
        if (connectionPool != null) {
            connectionPool.closeConnection(connection);
        }
        Logger.log(Level.DEBUG, "Put connection to connection pool.");
    }
}
