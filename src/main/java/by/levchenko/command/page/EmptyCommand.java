package by.levchenko.command.page;


import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.utility.PageEnum;

import javax.servlet.http.HttpServletRequest;

/**
 * A command that is executed when the user makes a wrong action.
 *
 * @author Vladislav Levchenko
 */

public class EmptyCommand implements ActionCommand {

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
        return page;
    }
}