package by.levchenko.command.page;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.utility.PageEnum;

import javax.servlet.http.HttpServletRequest;

public class DefinePageCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());

        String pageParam = request.getParameter("page");

        if (pageParam == null) {
            return page;
        }

        switch (pageParam) {
            case "login":
                page = ConfigurationManager.getProperty(PageEnum.LOGIN.getPage());
                break;
            case "register":
                page = ConfigurationManager.getProperty(PageEnum.REGISTER.getPage());
                break;
            case "home":
                page = ConfigurationManager.getProperty(PageEnum.HOME.getPage());
                break;
            default:
                return null;
        }

        return page;
    }
}
