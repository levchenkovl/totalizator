package by.levchenko.command.coeff;

import by.levchenko.command.ActionCommand;
import by.levchenko.command.manager.ConfigurationManager;
import by.levchenko.command.manager.MessageManager;
import by.levchenko.command.sport.PrintSportsCommand;
import by.levchenko.command.utility.PageEnum;
import by.levchenko.command.utility.UtilCommand;
import by.levchenko.database.entity.User;
import by.levchenko.logic.action.CoeffAction;
import by.levchenko.logic.exception.CoeffException;
import by.levchenko.command.transaction.TransactionManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * The command that is executed when the bookmaker edits the coefficient.
 *
 * @author Vladislav Levchenko
 */

public class EditCoeffCommand implements ActionCommand {

    private static Logger Logger = LogManager.getLogger(EditCoeffCommand.class);

    /**
     * @param request request received by the controller.
     * @return the page on which the controller redirects the user.
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = UtilCommand.definePage(request);

        User user = (User) request.getSession().getAttribute("user");

        String valueParam = request.getParameter("value");
        String expOwnerParam = request.getParameter("expOwner");
        String expGuestParam = request.getParameter("expGuest");
        String coeffIdParam = request.getParameter("coeffId");
        String creatorUsername = user.getUsername();

        if (page == null || valueParam == null
                || coeffIdParam == null) {
            page = ConfigurationManager.getProperty(PageEnum.INDEX.getPage());
            return page;
        }

        int coeffId = 0;
        float value = 0;
        int expOwner = 0;
        int expGuest = 0;
        try {
            /////////////////////////////////////////////////////////////////////////ввод символа вместо числа
            coeffId = !coeffIdParam.isEmpty() ? Integer.parseInt(coeffIdParam) : 0;
            value = Float.parseFloat(valueParam);
            if (expOwnerParam != null && expGuestParam != null) {
                expOwner = Integer.parseInt(expOwnerParam);
                expGuest = Integer.parseInt(expGuestParam);
            }
        } catch (NumberFormatException e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            Logger.log(Level.ERROR, e);
            return page;
        }


        try (TransactionManager transactionManager = new TransactionManager()) {
            transactionManager.setAutoCommit(false);

            CoeffAction coeffAction = new CoeffAction(transactionManager.getConnection());
            if (coeffAction.updateCoeffById(coeffId, value, expOwner, expGuest, creatorUsername)) {
                transactionManager.commit();

                request.setAttribute("status", MessageManager.getProperty("message.success"));
            } else {
                request.setAttribute("status", MessageManager.getProperty("message.cantEditCoeff"));
            }
        } catch (CoeffException e) {
            request.setAttribute("status", MessageManager.getProperty("message.cantEditCoeff"));
            Logger.log(Level.WARN, e);
        } catch (InterruptedException e) {
            request.setAttribute("status", MessageManager.getProperty("message.dbError"));
            Logger.log(Level.ERROR, e);
        } catch (Exception e) {
            request.setAttribute("status", MessageManager.getProperty("message.fail"));
            e.printStackTrace();
            Logger.log(Level.ERROR, e);
        }

        new PrintSportsCommand().execute(request);

        return page;
    }
}
