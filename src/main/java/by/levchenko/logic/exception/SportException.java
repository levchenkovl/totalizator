package by.levchenko.logic.exception;

public class SportException extends Exception {
    public SportException() {
        super();
    }

    public SportException(String message) {
        super(message);
    }

    public SportException(String message, Throwable cause) {
        super(message, cause);
    }

    public SportException(Throwable cause) {
        super(cause);
    }
}
