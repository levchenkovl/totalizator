package by.levchenko.logic.exception;

public class CoeffException extends Exception {
    public CoeffException() {
        super();
    }

    public CoeffException(String message) {
        super(message);
    }

    public CoeffException(String message, Throwable cause) {
        super(message, cause);
    }

    public CoeffException(Throwable cause) {
        super(cause);
    }
}
