package by.levchenko.logic.entity;


import by.levchenko.database.entity.Bet;

/**
 * An entity extended SportJSP to transfer information about bets to jsp.
 *
 * @author Vladislav Levchenko
 * @see by.levchenko.logic.entity.SportForJSP
 */

public class BetForJSP extends SportForJSP {

    /**
     * An entity for storing information from a database's table 'bet'
     */
    private Bet bet;

    public BetForJSP() {
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BetForJSP betForJSP = (BetForJSP) o;

        return bet != null ? bet.equals(betForJSP.bet) : betForJSP.bet == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (bet != null ? bet.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BetForJSP{" +
                "bet=" + bet +
                '}';
    }
}
