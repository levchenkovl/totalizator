package by.levchenko.logic.entity;


import by.levchenko.database.entity.Coefficient;
import by.levchenko.database.entity.Match;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * An entity to transfer information about matches with coefficients to jsp.
 *
 * @author Vladislav Levchenko
 */

public class SportForJSP implements Serializable, Cloneable {

    /**
     * An entity for storing information from a database's table 'match'.
     */
    private Match match;

    /**
     * List of entities for storing information from a database's table 'coefficient'.
     */
    private List<Coefficient> coefficients;

    public SportForJSP() {
        coefficients = new ArrayList<>();
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public List<Coefficient> getCoefficients() {
        return coefficients;
    }

    public void addCoefficient(Coefficient coefficient) {
        coefficients.add(coefficient);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SportForJSP that = (SportForJSP) o;

        if (match != null ? !match.equals(that.match) : that.match != null) return false;
        return coefficients != null ? coefficients.equals(that.coefficients) : that.coefficients == null;
    }

    @Override
    public int hashCode() {
        int result = match != null ? match.hashCode() : 0;
        result = 31 * result + (coefficients != null ? coefficients.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SportForJSP{" +
                "match=" + match +
                ", coefficients=" + coefficients +
                '}';
    }
}
