package by.levchenko.logic.action;

import by.levchenko.database.connectionpool.ProxyConnection;
import by.levchenko.database.dao.*;
import by.levchenko.database.entity.*;
import by.levchenko.logic.entity.SportForJSP;
import by.levchenko.logic.exception.SportException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Methods that promise functionality for working with matches.
 *
 * @author Vladislav Levchenko
 */


public class SportAction extends Action {

    private static Logger Logger = LogManager.getLogger(CoeffAction.class);


    public SportAction(ProxyConnection connection) {
        super(connection);
        Logger.log(Level.DEBUG, "Init SportAction.");
    }

    /**
     * If isBet is true the method looks for all matches with information about lasts coefficients on this matches.
     * If isBet is false the method looks for all matches with information about all coefficients on this matches.
     *
     * @param isBet defines what the method looks for.
     * @return returns list of matches.
     */
    public ArrayList<SportForJSP> findAll(boolean isBet) {
        ArrayList<SportForJSP> sportForJSPS = new ArrayList<>();

        MatchDAO matchDAO = new MatchDAO(connection);
        List<Match> matches = matchDAO.findAll();

        matches = matches.stream().filter(match -> !match.isFinished() || isBet).collect(Collectors.toList());

        CoefficientDAO coefficientDAO = new CoefficientDAO(connection);
        ArrayList<Coefficient> coefficients = (ArrayList<Coefficient>) coefficientDAO.findAll();

        matches.forEach(match -> {
            SportForJSP sportForJSP = new SportForJSP();

            sportForJSP.setMatch(match);
            coefficients.forEach(coefficient -> {
                if (match.getId() == coefficient.getMatchId() && (coefficient.isAvailable())) {
                    sportForJSP.addCoefficient(coefficient);
                }
            });

            sportForJSPS.add(sportForJSP);
        });

        return sportForJSPS;
    }

    /**
     * The method updates match with new values.
     *
     * @param matchId     updated match id.
     * @param sportName   new kind of sport.
     * @param owner       new first member's name.
     * @param guest       new second member's name.
     * @param nameCreator name of match creator.
     * @return true if the match is updated successfully.
     */
    public boolean updateMatchById(int matchId, String sportName, String owner, String guest, String nameCreator) {
        MatchDAO matchDAO = new MatchDAO(connection);

        Match match = matchDAO.findEntityById(matchId);
        MemberDAO memberDAO = new MemberDAO(connection);

        if (!match.getOwnerName().equalsIgnoreCase(owner)) {
            Member member = new Member();
            member.setName(owner);
            if (memberDAO.findEntityByName(owner) == null) {
                memberDAO.create(member);
            }
        }

        if (!match.getGuestName().equalsIgnoreCase(guest)) {
            Member member = new Member();
            member.setName(guest);
            if (memberDAO.findEntityByName(guest) == null) {
                memberDAO.create(member);
            }
        }

        match.setOwnerName(owner);
        match.setGuestName(guest);
        match.setNameCreator(nameCreator);
        match.setSportName(sportName);

        if (matchDAO.update(match) == null) {
            return false;
        }
        return true;
    }

    /**
     * The method looks for matches created by user.
     *
     * @param id creator id.
     * @return list of matches.
     */
    public List<SportForJSP> findSportsByUserId(int id) {
        UserDAO userDAO = new UserDAO(connection);
        User user = userDAO.findEntityById(id);

        List<SportForJSP> sportForJSPS = findAll(false);

        sportForJSPS = sportForJSPS.stream().filter(sportForJSP -> (sportForJSP.getMatch() != null
                && sportForJSP.getMatch().getNameCreator() != null
                && sportForJSP.getMatch().getNameCreator().equals(user.getUsername()))).collect(Collectors.toList());

        return sportForJSPS;
    }

    /**
     * The method delete matches by id.
     *
     * @param id deleted match id.
     * @return true if match is deleted successfully.
     */

    public boolean deleteSportById(int id) {
        CoeffAction coeffAction = new CoeffAction(connection);
        CoefficientDAO coefficientDAO = new CoefficientDAO(connection);
        List<Coefficient> coefficients = coefficientDAO.findAll();

        for (Coefficient coefficient : coefficients) {
            if (coefficient.getMatchId() == id) {
                coeffAction.deleteCoeffById(coefficient.getId());//////////////////////чейнОфРеспонс
            }
        }

        MatchDAO matchDAO = new MatchDAO(connection);
        matchDAO.delete(id);
        return true;
    }

    /**
     * The method creates new match.
     *
     * @param creatorName creator's name.
     * @param sportName   kind of sport.
     * @param owner       first member's name.
     * @param guest       second member's name.
     * @return returns true if match is created successfully.
     * @throws SportException
     */
    public boolean addSport(String creatorName, String sportName, String owner, String guest) throws SportException {
        MatchDAO matchDAO = new MatchDAO(connection);
        Match match = new Match();

        match.setSportName(sportName);
        match.setOwnerName(owner);
        match.setGuestName(guest);
        match.setNameCreator(creatorName);

        MemberDAO memberDAO = new MemberDAO(connection);
        if (!createMember(memberDAO, match, owner)) {
            throw new SportException("Cannot create owner.");
        }
        if (!createMember(memberDAO, match, guest)) {
            throw new SportException("Cannot create guest.");
        }

        if (matchDAO.create(match)) {
            int matchId = matchDAO.defineMaxId();
            CoeffAction coeffAction = new CoeffAction(connection);

            coeffAction.createCoeffForMatch(matchId, 1, null, null, null, "owner");
            coeffAction.createCoeffForMatch(matchId, 1, null, null, null, "owner");
            coeffAction.createCoeffForMatch(matchId, 1, null, null, null, "owner");
            coeffAction.createCoeffForMatch(matchId, 1, 0, 0, null, "owner");
            coeffAction.createCoeffForMatch(matchId, 1, 0, 0, null, "owner");
            return true;
        }
        throw new SportException("Cannot add sport.");

    }

    /**
     * The method creates member for addSport.
     *
     * @param memberDAO
     * @param match
     * @param name
     * @return returns true if member is created successfully.
     */
    private boolean createMember(MemberDAO memberDAO, Match match, String name) {
        if (memberDAO.findEntityByName(name) != null) {
            match.setOwnerName(name);
        } else {
            Member member = new Member();
            member.setName(name);
            if (!memberDAO.create(member)) {
                return false;
            }
        }
        return true;
    }

    /**
     * The method define randomly results of match.
     *
     * @param matchId match id.
     */

    public void defineResultMatch(int matchId) {
        MatchDAO matchDAO = new MatchDAO(connection);
        BetDAO betDAO = new BetDAO(connection);
        UserDAO userDAO = new UserDAO(connection);
        CoefficientDAO coefficientDAO = new CoefficientDAO(connection);
        List<Bet> bets = null;
        List<Coefficient> coefficients = null;
        Random random = new Random(new Date().getTime());

        bets = betDAO.findAll();
        coefficients = coefficientDAO.findAll();
        Match match = matchDAO.findEntityById(matchId);
        int ownerRes = random.nextInt(10);
        int guestRes = random.nextInt(10);
        match.setOwnerResult(ownerRes);
        match.setGuestResult(guestRes);
        match.setFinished(true);
        List<Coefficient> finalCoefficients = coefficients.stream()
                .filter(coefficient -> coefficient.getMatchId() == match.getId()).collect(Collectors.toList());
        bets.forEach(bet -> finalCoefficients.forEach(coefficient -> {
            if (bet.getCoefficientId() == coefficient.getId()) {
                if (ownerRes - guestRes > 0 && coefficient.getCategoryCoeff().equals("owner") ||
                        ownerRes - guestRes == 0 && coefficient.getCategoryCoeff().equals("draw") ||
                        ownerRes - guestRes < 0 && coefficient.getCategoryCoeff().equals("guest") ||
                        coefficient.getCategoryCoeff().equals("result") &&
                                coefficient.getExpOwner() == ownerRes && coefficient.getExpGuest() == guestRes) {

                    bet.setGain(bet.getValue().multiply(BigDecimal.valueOf(coefficient.getValue())));
                    User user = userDAO.findEntityById(bet.getUserId());
                    user.setBalance(user.getBalance().add(bet.getGain()));
                    userDAO.update(user);
                } else {
                    bet.setGain(BigDecimal.valueOf(0));
                }
                bet.setTime(new Date());
                betDAO.update(bet);
            }
        }));
        matchDAO.update(match);
    }
}
