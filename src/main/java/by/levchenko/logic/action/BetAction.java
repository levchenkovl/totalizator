package by.levchenko.logic.action;

import by.levchenko.database.connectionpool.ProxyConnection;
import by.levchenko.database.dao.BetDAO;
import by.levchenko.database.dao.UserDAO;
import by.levchenko.database.entity.Bet;
import by.levchenko.database.entity.Coefficient;
import by.levchenko.database.entity.User;
import by.levchenko.logic.entity.BetForJSP;
import by.levchenko.logic.entity.SportForJSP;
import by.levchenko.logic.exception.BetException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Methods that promise functionality for working with bets.
 *
 * @author Vladislav Levchenko
 */

public class BetAction extends Action {


    private static Logger Logger = LogManager.getLogger(BetAction.class);

    public BetAction(ProxyConnection connection) {
        super(connection);
        Logger.log(Level.DEBUG, "Init BetAction.");
    }

    /**
     * The method looks for bets made by the client.
     *
     * @param id user id who made bets.
     * @return list of bets.
     */
    public List<BetForJSP> findBetForJSPByUserId(int id) {
        SportAction sportAction = new SportAction(connection);
        List<SportForJSP> sportForJSPS = sportAction.findAll(true);
        List<BetForJSP> betForJSPS = new ArrayList<>();
        List<Bet> bets = null;
        BetDAO betDAO = new BetDAO(connection);
        bets = betDAO.findAll().stream().filter(bet -> bet.getUserId() == id).collect(Collectors.toList());
        for (SportForJSP sportForJSP : sportForJSPS) {
            for (Bet bet : bets) {
                for (Coefficient coefficient : sportForJSP.getCoefficients()) {
                    if (bet.getCoefficientId() == coefficient.getId()) {
                        BetForJSP betForJSP = new BetForJSP();
                        betForJSP.setMatch(sportForJSP.getMatch());
                        betForJSP.addCoefficient(coefficient);
                        betForJSP.setBet(bet);
                        betForJSPS.add(betForJSP);
                    }
                }
            }
        }
        return betForJSPS;
    }

    /**
     * The method creates bet.
     *
     * @param value         bet amount.
     * @param coefficientId coefficient id on which user makes bet.
     * @param user          user who makes bet.
     * @return true if bet is created.
     * @throws BetException
     */

    public boolean doBet(BigDecimal value, int coefficientId, User user) throws BetException {
        BetDAO betDAO = new BetDAO(connection);

        Bet bet = new Bet();
        bet.setValue(value);
        bet.setCoefficientId(coefficientId);
        bet.setUserId(user.getId());
        if (user.getBalance().subtract(value).compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new BetException("Not enough balance.");
        }
        if (betDAO.create(bet)) {
            user.setBalance(user.getBalance().subtract(value));
            UserDAO userDAO = new UserDAO(connection);
            userDAO.update(user);
            return true;
        }
        throw new BetException("Cannot do bet.");
    }
}
