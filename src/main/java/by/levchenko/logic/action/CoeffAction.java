package by.levchenko.logic.action;

import by.levchenko.database.connectionpool.ProxyConnection;
import by.levchenko.database.dao.BetDAO;
import by.levchenko.database.dao.CategoryCoeffDAO;
import by.levchenko.database.dao.CoefficientDAO;
import by.levchenko.database.dao.UserDAO;
import by.levchenko.database.entity.Bet;
import by.levchenko.database.entity.CategoryCoeff;
import by.levchenko.database.entity.Coefficient;
import by.levchenko.database.entity.User;
import by.levchenko.logic.entity.SportForJSP;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Methods that promise functionality for working with coefficients.
 *
 * @author Vladislav Levchenko
 */


public class CoeffAction extends Action {

    private static Logger Logger = LogManager.getLogger(CoeffAction.class);

    public CoeffAction(ProxyConnection connection) {
        super(connection);
        Logger.log(Level.DEBUG, "Init CoeffAction.");
    }

    /**
     * The method looks matches with coefficients made by the bookmaker.
     *
     * @param userId bookmaker id who made coefficient.
     * @return list of matches.
     */
    public List<SportForJSP> findCoefficientsByUserId(int userId) {
        List<SportForJSP> sportForJSPS = new ArrayList<>();
        SportAction sportAction = new SportAction(connection);
        sportForJSPS = sportAction.findAll(false);
        UserDAO userDAO = new UserDAO(connection);

        sportForJSPS = sportForJSPS.stream().filter(sportForJSP -> {
            User user = null;
            user = userDAO.findEntityById(userId);

            for (Coefficient coefficient : sportForJSP.getCoefficients()) {
                if (coefficient != null && coefficient.getNameCreator() != null && coefficient.getNameCreator().equals(user.getUsername())) {
                    return true;
                }
            }
            return false;

        }).collect(Collectors.toList());

        return sportForJSPS;
    }

    public boolean createCoeffForMatch(int matchId, float value, Integer expOwner, Integer expGuest, String creatorUsername, String categoryCoeff) {
        CoefficientDAO coefficientDAO = new CoefficientDAO(connection);
        Coefficient coefficient = null;
        coefficient = new Coefficient();
        coefficient.setValue(value);
        coefficient.setExpOwner(expOwner);
        coefficient.setExpGuest(expGuest);
        coefficient.setNameCreator(creatorUsername);
        coefficient.setCategoryCoeff(categoryCoeff);
        coefficient.setMatchId(matchId);
        coefficient.setAvailable(true);
        return coefficientDAO.create(coefficient);
    }

    /**
     * The method updates coefficient's value and members results.
     *
     * @param coeffId         updated coefficient id.
     * @param value           new coefficient's value.
     * @param expOwner        new expected result of first member.
     * @param expGuest        new expected result of second member.
     * @param creatorUsername bookmaker's name who updates coefficient.
     * @return true if coefficient is updated successfully.
     */
    public boolean updateCoeffById(int coeffId, float value, Integer expOwner, Integer expGuest, String creatorUsername) {
        boolean b = true;
        CoefficientDAO coefficientDAO = new CoefficientDAO(connection);
        Coefficient coefficient = coefficientDAO.findEntityById(coeffId);

        ArrayList<Bet> bets = (ArrayList<Bet>) new BetDAO(connection).findAll();
        if (bets != null) {
            if (bets.stream().filter(bet -> bet.getCoefficientId() == coeffId).collect(Collectors.toList()).isEmpty()) {
                b = false;
            }
        } else {
            b = false;
        }

        if (b) {
            coefficient.setAvailable(false);
            if (coefficientDAO.update(coefficient) == null) {
                return false;
            }

            coefficient.setValue(value);
            coefficient.setExpOwner(expOwner);
            coefficient.setExpGuest(expGuest);
            coefficient.setAvailable(true);
            coefficient.setNameCreator(creatorUsername);
            if (!coefficientDAO.create(coefficient)) {
                return false;
            }
        } else {
            coefficient.setValue(value);
            coefficient.setExpOwner(expOwner);
            coefficient.setExpGuest(expGuest);
            coefficient.setNameCreator(creatorUsername);
            if (coefficientDAO.update(coefficient) == null) {
                return false;
            }
        }

        return true;
    }

    /**
     * The method deletes coefficient by coefficient id and all bets on this coefficients.
     *
     * @param id deleted coefficient id.
     * @return true if coefficient is updated successfully.
     */
    public boolean deleteCoeffById(int id) {
        CoefficientDAO coefficientDAO = new CoefficientDAO(connection);
        Coefficient coefficient = coefficientDAO.findEntityById(id);
        BetDAO betDAO = new BetDAO(connection);
        List<Bet> bets = betDAO.findAll();
        bets = bets.stream().filter(bet -> bet.getCoefficientId() == id).collect(Collectors.toList());
        UserDAO userDAO = new UserDAO(connection);
        for (Bet bet : bets) {
            User user = userDAO.findEntityById(bet.getUserId());
            user.setBalance(user.getBalance().add(bet.getValue()));
            userDAO.update(user);
            new BetDAO(connection).delete(id);
        }
        coefficient.setAvailable(false);
        coefficientDAO.update(coefficient);
        return true;
    }

    /**
     * The method deletes bet by id.
     *
     * @param id bet id.
     * @return true if bet is deleted successfully.
     */
    public boolean deleteBetById(int id) {
        BetDAO betDAO = new BetDAO(connection);
        List<Bet> bets = betDAO.findAll();
        int coefficientId = betDAO.findEntityById(id).getCoefficientId();
        boolean coeffWithoutBet = true;
        if (!new CoefficientDAO(connection).findEntityById(coefficientId).isAvailable()) {
            for (Bet bet : bets) {
                if (bet.getCoefficientId() == coefficientId) {
                    coeffWithoutBet = false;
                    break;
                }
            }
            if (coeffWithoutBet) {
                deleteCoeffById(coefficientId);
            }
        }

        return betDAO.delete(id);
    }

    /**
     * The method looks for list kind of coefficients.
     *
     * @return list of kind of coefficients.
     */
    public List<CategoryCoeff> findAllCategoryCoeff() {
        List<CategoryCoeff> categoryCoeffs = new ArrayList<>();
        CategoryCoeffDAO categoryCoeffDAO = new CategoryCoeffDAO(connection);
        categoryCoeffs = categoryCoeffDAO.findAll();
        return categoryCoeffs;
    }
}
