package by.levchenko.logic.action;

import by.levchenko.database.connectionpool.ProxyConnection;

public class Action {

    /**
     * Connection for accessing the database.
     */
    protected ProxyConnection connection;

    protected Action(ProxyConnection connection) {
        this.connection = connection;
    }
}
