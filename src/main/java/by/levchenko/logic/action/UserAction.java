package by.levchenko.logic.action;

import by.levchenko.database.connectionpool.ProxyConnection;
import by.levchenko.database.dao.BetDAO;
import by.levchenko.database.dao.UserDAO;
import by.levchenko.database.entity.Bet;
import by.levchenko.database.entity.User;
import by.levchenko.database.entity.UserRole;
import by.levchenko.logic.LogicUtil;
import by.levchenko.logic.exception.UserException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Methods that promise functionality for working with users.
 *
 * @author Vladislav Levchenko
 */

public class UserAction extends Action {

    private static Logger Logger = LogManager.getLogger(CoeffAction.class);

    private static final String KEY = "Bar12345Bar12345"; // 128 bit KEY
    private static final String INIT_VECTOR = "RandomInitVector"; // 16 bytes IV

    public UserAction(ProxyConnection connection) {
        super(connection);
        Logger.log(Level.DEBUG, "Init UserAction.");
    }

    /**
     * The method checks the password and login to authentication and authorization.
     *
     * @param username username of user's account.
     * @param password password of user's account.
     * @return returns user object.
     * @throws UserException
     */
    public User checkLoginAndPass(String username, String password) throws UserException {
        User user = null;
        UserDAO userDAO = new UserDAO(connection);
        user = userDAO.findEntityByUsername(username);

        if (user != null && checkPassword(password, user.getPassword())) {
            return user;
        }
        throw new UserException("Incorrect login or password.");
    }

    /**
     * The method changes user's password.
     *
     * @param user         user whose password is changed.
     * @param new_password new user's password.
     * @return returns true if password is changed successfully.
     * @throws UserException
     */
    public boolean changePassword(User user, String new_password) throws UserException {
        try {
            UserDAO userDAO = new UserDAO(connection);
            user.setPassword(LogicUtil.createEncryptedPassword(new_password));
            if (userDAO.update(user) != null) {
                return true;
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            Logger.log(Level.ERROR, e);
        }
        throw new UserException("Cannot change password.");
    }

    /**
     * The method parse confirmation string to user's field.
     *
     * @param newUser    confirmation string.
     * @param newUserMap map of user's fields.
     * @return returns map of user's fields.
     */
    private boolean parseNewUser(String newUser, Map newUserMap) {
        String[] fields = newUser.split("=|;");
        if (fields[0].equals("username") && fields[1] != null &&
                fields[2].equals("password") && fields[3] != null &&
                fields[4].equals("email") && fields[5] != null &&
                fields[6].equals("role") && fields[7] != null &&
                fields[8].equals("date") && fields[9] != null) {
            newUserMap.put("username", fields[1]);
            newUserMap.put("password", fields[3]);
            newUserMap.put("email", fields[5]);
            newUserMap.put("role", fields[7]);
            newUserMap.put("date", fields[9]);
            return true;
        }
        return false;
    }

    /**
     * The method register new user.
     *
     * @param newUser confirmation string from email.
     * @return returns true if user is registered successfully.
     * @throws UserException
     */
    public boolean register(String newUser) throws UserException {
        Map<String, String> newUserMap = new HashMap();

        newUser = decrypt(KEY, INIT_VECTOR, newUser);
        if (!parseNewUser(newUser, newUserMap)) {
            return false;
        }

        String username = newUserMap.get("username");
        String password = newUserMap.get("password");
        String email = newUserMap.get("email");
        String role = newUserMap.get("role");
        String date = newUserMap.get("date");
        BigDecimal startBalance = BigDecimal.valueOf(1000);

        if (new Date().getTime() - Long.decode(date) > TimeUnit.MINUTES.toMillis(15) ||
                loginAndEmailIsExist(username, email)) {
            return false;
        }


        User user = new User();
        try {
            user.setUsername(username);
            user.setPassword(LogicUtil.createEncryptedPassword(password));
            user.setEmail(email);
            user.setRole(role != null && !role.equals("null") ? UserRole.valueOf(role.toUpperCase()) : UserRole.CLIENT);
            user.setBalance(!user.getRole().equals(UserRole.CLIENT) ? null : startBalance);
            UserDAO userDAO = new UserDAO(connection);
            return userDAO.create(user);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            Logger.log(Level.ERROR, e);
        }
        throw new UserException("Cannot register new user");
    }

    /**
     * The method encrypts the string to send an e-mail to confirm the registration.
     *
     * @param key        cryptography key.
     * @param initVector initialization vector.
     * @param value      not encrypted password.
     * @return returns encrypted string.
     * @throws UnsupportedEncodingException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private static String encrypt(String key, String initVector, String value) throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

        byte[] encrypted = cipher.doFinal(value.getBytes());

        return Base64.getEncoder().encodeToString(encrypted);

    }

    /**
     * The method decrypt confirmation string.
     *
     * @param key        cryptography key.
     * @param initVector initialization vector.
     * @param encrypted  encrypted string.
     * @return
     */
    private static String decrypt(String key, String initVector, String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));

            return new String(original);
        } catch (Exception e) {
            Logger.log(Level.ERROR, e);
        }

        return null;
    }

    /**
     * The method sends confirmation string an email.
     *
     * @param username username for registration.
     * @param password password for registration.
     * @param email    email for registration.
     * @param role     role for registration.
     * @return returns true if confirmation string is sent successfully.
     * @throws UserException
     */
    public boolean sendRegister(String username, String password, String email, String role) throws UserException {
        String subject = "Regisret in totalizator";
        Date date = new Date();
        String body = "To complete the authorization, click on the link \n http://localhost:8080/controller?command=register&newUser=";
        String string = "username=" + username + ";password=" + password + ";email=" + email + ";role=" + role + ";" +
                "date=" + date.getTime();

        try {
            string = encrypt(KEY, INIT_VECTOR, string);

            body += string;

            return sendMailAdvance(email, subject, body);

        } catch (UnsupportedEncodingException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            Logger.log(Level.ERROR, e);
        }

        return true;
    }

    /**
     * The method to send letter.
     *
     * @param emailTo destination email.
     * @param subject letter's subject.
     * @param body    letter's body.
     * @return returns true if letter is sent successfully.
     */
    private boolean sendMailAdvance(String emailTo, String subject, String body) {
        String host = "smtp.gmail.com";
        String userName = "demigoodlike@gmail.com";
        String password = "demigoodlike1566138";
        String port = "587";
        String starttls = "true";

        try {
            Properties props = null;
            props = System.getProperties();
            props.put("mail.smtp.user", userName);
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.debug", "true");
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.socketFactory.port", port);
            props.put("mail.smtp.starttls.enable", starttls);
            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userName, password);
                }
            });
            session.setDebug(true);

            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(userName));
            msg.setSubject(subject);
            msg.setText(body, "ISO-8859-1");
            msg.setSentDate(new Date());
            msg.setHeader("content-Type", "text/html;charset=\"ISO-8859-1\"");
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
            msg.saveChanges();

            javax.mail.Transport transport = session.getTransport("smtp");
            transport.connect(host, userName, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();

            return true;
        } catch (Exception e) {
            Logger.log(Level.ERROR, e);
            return false;
        }
    }


    /**
     * The method checks username and email for existence.
     *
     * @param username username for check.
     * @param email    email for check.
     * @return returns true if login or email exists.
     */
    public boolean loginAndEmailIsExist(String username, String email) {
        UserDAO userDAO = new UserDAO(connection);
        List<User> users = userDAO.findAll();
        users = users.stream().filter(user -> user.getUsername().equals(username) || user.getEmail().equals(email)).collect(Collectors.toList());
        return users.size() > 0;
    }

    /**
     * The method verifies the password pattern.
     *
     * @param pass  password for verification.
     * @param regex pattern of password.
     * @return returns true if password is verified.
     */
    public boolean checkWithRegexp(String pass, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pass);
        return matcher.matches();
    }

    /**
     * The method looks for list of all users.
     *
     * @return returns list of users.
     */
    public ArrayList<User> findAll() {
        List<User> users = new ArrayList<>();
        UserDAO userDAO = null;
        userDAO = new UserDAO(connection);
        users = userDAO.findAll();
        return (ArrayList<User>) users;
    }

    /**
     * The method looks for user by id.
     *
     * @param id user id.
     * @return returns user object.
     */
    public User findUserById(int id) {
        User user = null;
        UserDAO userDAO = null;
        userDAO = new UserDAO(connection);
        user = userDAO.findEntityById(id);
        return user;
    }

    /**
     * The method deletes user.
     *
     * @param userId user id.
     * @param role   user's role to deletes relevant objects.
     * @return returns true if user is deleted successfully.
     */
    public boolean deleteUser(int userId, UserRole role) {
        UserDAO userDAO = new UserDAO(connection);

        switch (role) {
            case ADMINISTRATOR:
                return userDAO.delete(userId);
            case BOOKMAKER:
                return userDAO.delete(userId);
            case CLIENT:
                BetDAO betDAO = new BetDAO(connection);
                List<Bet> bets = betDAO.findEntityByUserId(userId);
                CoeffAction coeffAction = new CoeffAction(connection);
                bets.forEach(bet -> coeffAction.deleteBetById(bet.getId()));
                return userDAO.delete(userId);
            default:
                return false;
        }
    }

    /**
     * The method checks password for correctness.
     *
     * @param password       entered password
     * @param passwordFromDB password saved id database.
     * @return returns true if passwords match.
     * @throws UserException
     */
    public boolean checkPassword(String password, String passwordFromDB) throws UserException {
        try {
            return LogicUtil.createEncryptedPassword(password).equals(passwordFromDB);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            Logger.log(Level.ERROR, e);
        }
        throw new UserException("Cannot check password");
    }
}
