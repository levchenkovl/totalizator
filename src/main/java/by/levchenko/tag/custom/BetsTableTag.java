package by.levchenko.tag.custom;

import by.levchenko.logic.entity.BetForJSP;
import by.levchenko.tag.manager.PageContentManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A tag for displaying table of bets.
 *
 * @author Vladislav Levchenko
 */

public class BetsTableTag extends TagSupport {
    private ArrayList<BetForJSP> bets;
    private String userRole;
    private String locale;

    public void setBets(ArrayList<BetForJSP> bets) {
        this.bets = bets;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            if (locale.equals("ru_RU") || locale.equals("en_US") || locale.equals("be_BY")) {
                PageContentManager.changeLocale(locale);
            }

            String matchId = PageContentManager.getProperty("matchId");
            String time = PageContentManager.getProperty("sport.bet.time");
            String nameSport = PageContentManager.getProperty("sport.nameSport");
            String members = PageContentManager.getProperty("sport.members");
            String coeffValue = PageContentManager.getProperty("sport.coeffValue");
            String gain = PageContentManager.getProperty("sport.gain");
            String nameCreatorCoefficient = PageContentManager.getProperty("sport.nameCreatorCoefficient");
            String nameCreatorMatch = PageContentManager.getProperty("sport.nameCreatorMatch");
            String notyet = PageContentManager.getProperty("sport.bet.notyet");
            String admin = "ADMINISTRATOR";

            int count = userRole.equals(admin) ? 9 : 7;
            JspWriter out = pageContext.getOut();
            out.write("<table class='table-database-tables display'>");
            out.write("<thead><tr>");
            for (int i = 0; i < count; i++) {
                out.write("<th></th>");
            }
            out.write("</tr></thead><tbody>");

            for (BetForJSP bet : bets) {
                out.write("<tr>");
                out.write("<th>" + matchId + "</th>");
                out.write("<th>" + time + "</th>");
                out.write("<th>" + nameSport + "</th>");
                out.write("<th>" + members + "</th>");
                out.write("<th>" + bet.getCoefficients().get(0).getCategoryCoeff() + "</th>");
                out.write("<th>" + coeffValue + "</th>");
                out.write("<th>" + gain + "</th>");
                if (userRole.equals(admin)) {
                    out.write("<th>" + nameCreatorCoefficient + "</th>");
                    out.write("<th>" + nameCreatorMatch + "</th>");
                }

                out.write("</tr><tr>");

                out.write("<td>" + bet.getMatch().getId() + "</td>");
                out.write("<td>" + (bet.getMatch().isFinished() ? bet.getBet().getTime() : notyet) + "</td>");
                out.write("<td>" + notNullString(bet.getMatch().getSportName()) + "</td>");

                out.write("<td>" + notNullString(bet.getMatch().getOwnerName()) + " - " + notNullString(bet.getMatch().getGuestName()) +
                        (bet.getMatch().isFinished() ?
                                "<br/>" + bet.getMatch().getOwnerResult() + ":" + bet.getMatch().getGuestResult() : "") + "</td>");

                out.write("<td>" + (bet.getCoefficients().get(0).getCategoryCoeff().equals("result") ?
                        bet.getCoefficients().get(0).getExpOwner() + " : " +
                                bet.getCoefficients().get(0).getExpGuest() + " | " : "") +
                        bet.getCoefficients().get(0).getValue() + "</td>");

                out.write("<td>" + notNullString(String.valueOf(bet.getBet().getValue())) + "</td>");
                out.write("<td>" + notNullString(String.valueOf(bet.getBet().getGain())) + "</td>");
                if (userRole.equals(admin)) {
                    out.write("<td>" + notNullString(bet.getCoefficients().get(0).getNameCreator()) + "</td>");
                    out.write("<td>" + notNullString(bet.getMatch().getNameCreator()) + "</td>");
                }
                out.write("</tr>");
            }
            out.write("</tbody></table>");


        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }

    private String notNullString(String s) {
        return s == null || s.equals("null") ? "" : s;
    }
}
