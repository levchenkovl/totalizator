package by.levchenko.tag.manager;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The class extracts information from the pagecontent.properties file.
 *
 * @author Vladislav Levchenko
 */

public class PageContentManager {

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("pagecontent");

    public static void changeLocale(String locale) {
        Locale bundleLocale = new Locale(locale.substring(0, 2), locale.substring(3, 5));
        resourceBundle = ResourceBundle.getBundle("pagecontent", bundleLocale);
    }

    // класс извлекает информацию из файла messages_ru_RU.properties
    private PageContentManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}