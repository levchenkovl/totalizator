package by.levchenko;

import by.levchenko.database.connectionpool.ConnectionPool;
import by.levchenko.database.connectionpool.ProxyConnection;
import by.levchenko.database.dao.UserDAO;
import by.levchenko.database.entity.User;
import by.levchenko.logic.action.BetAction;
import by.levchenko.logic.exception.BetException;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Statement;

public class BetActionTest {

    private BetAction betAction;
    private ProxyConnection connection;
    private ConnectionPool connectionPool;
    private Statement statement;

    @BeforeTest
    public void init() throws InterruptedException, SQLException, IOException {
        connectionPool = ConnectionPool.getInstance(true);
        connection = connectionPool.getConnection();
        statement = connection.createStatement();

        TestDB.create(statement);

        betAction = new BetAction(connection);
    }

    @Test
    public void TestFindBetForJSPByUserId() throws SQLException {
        int expectedSize = 15;
        int actualSize = betAction.findBetForJSPByUserId(5).size();
        Assert.assertEquals(actualSize, expectedSize);
    }

    @Test(expectedExceptions = BetException.class)
    public void TestDoBetException() throws BetException {
        UserDAO userDAO = new UserDAO(connection);
        User user = userDAO.findEntityById(5);
        betAction.doBet(BigDecimal.valueOf(user.getBalance().intValue() + 1), 1, user);
    }

    @AfterTest
    public void dropSchema() throws SQLException {
        connectionPool.closeConnection(connection);
        TestDB.drop(statement);
        statement.close();
    }
}