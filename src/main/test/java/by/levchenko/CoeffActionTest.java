package by.levchenko;

import by.levchenko.database.connectionpool.ConnectionPool;
import by.levchenko.database.connectionpool.ProxyConnection;
import by.levchenko.logic.action.CoeffAction;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

public class CoeffActionTest {

    private CoeffAction coeffAction;
    private ProxyConnection connection;
    private ConnectionPool connectionPool;
    private Statement statement;

    @BeforeTest
    public void init() throws InterruptedException, IOException, SQLException {
        connectionPool = ConnectionPool.getInstance(true);
        connection = connectionPool.getConnection();
        statement = connection.createStatement();
        TestDB.create(statement);

        coeffAction = new CoeffAction(connection);
    }


    @Test
    public void findCoefficientsByUserIdTest() {
        int expectedSize = 12;
        int actualSize = coeffAction.findCoefficientsByUserId(3).size();
        Assert.assertEquals(actualSize, expectedSize);
    }


    @Test
    public void findAllCategoryCoeffTest() {
        int expectedSize = 4;
        int actualSize = coeffAction.findAllCategoryCoeff().size();
        Assert.assertEquals(actualSize, expectedSize);
    }

    @Test
    public void updateCoeffByIdTest() {
        boolean actual = coeffAction.updateCoeffById(15, 1.4f, 0, 0, "Lola");
        Assert.assertTrue(actual);
    }

    @Test
    public void deleteCoeffByIdTest() {
        boolean actual = coeffAction.deleteCoeffById(1);
        Assert.assertTrue(actual);
    }

    @Test
    public void deleteBetByIdTest() {
        boolean actual = coeffAction.deleteBetById(1);
        Assert.assertTrue(actual);
    }

    @AfterTest
    public void dropSchema() throws SQLException {
        connectionPool.closeConnection(connection);
        TestDB.drop(statement);
        statement.close();
    }
}
