package by.levchenko;

import by.levchenko.database.connectionpool.ConnectionPool;
import by.levchenko.database.connectionpool.ProxyConnection;
import by.levchenko.database.dao.UserDAO;
import by.levchenko.database.entity.User;
import by.levchenko.logic.action.UserAction;
import by.levchenko.logic.exception.UserException;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

public class UserActionTest {

    private UserAction userAction;
    private ProxyConnection connection;
    private ConnectionPool connectionPool;
    private Statement statement;

    @BeforeTest
    public void init() throws InterruptedException, IOException, SQLException {
        connectionPool = ConnectionPool.getInstance(true);
        connection = connectionPool.getConnection();
        statement = connection.createStatement();
        TestDB.create(statement);

        userAction = new UserAction(connection);
    }

    @Test(expectedExceptions = UserException.class)
    public void checkLoginAndPassTest() throws UserException {
        userAction.checkLoginAndPass("Max", "2");
    }

    @Test
    public void changePasswordTest() throws UserException {
        UserDAO userDAO = new UserDAO(connection);
        User user = userDAO.findEntityById(5);
        boolean actual = userAction.changePassword(user, "qwerty");
        Assert.assertTrue(actual);
    }

    @Test
    public void sendRegisterTest() throws UserException {
        boolean actual = userAction.sendRegister("Qwe", "Qwerty123", "qwerty123@gmail.com", "Bookmaker");
        Assert.assertTrue(actual);
    }

    @Test
    public void checkLoginTest() {
        boolean actual = userAction.loginAndEmailIsExist("Max", "Max2@gmail.com");
        Assert.assertTrue(actual);
    }

    @Test
    public void checkPassWithRegexpTest() {
        final String PATTENR_PASSWORD = "^(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*[\\d])\\S{8,}$";
        boolean actual = userAction.checkWithRegexp("QWerty123", PATTENR_PASSWORD);
        Assert.assertTrue(actual);
    }

    @Test
    public void findAllTest() {
        int expected = 8;
        int actual = userAction.findAll().size();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void findUserByIdTest() {
        boolean actual = userAction.findUserById(1) != null;
        Assert.assertTrue(actual);
    }

    @AfterTest
    public void dropSchema() throws SQLException {
        connectionPool.closeConnection(connection);
        TestDB.drop(statement);
        statement.close();
    }
}
