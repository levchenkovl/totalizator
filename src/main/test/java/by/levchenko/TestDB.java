package by.levchenko;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.stream.Collectors;

public class TestDB {

    private final static String filename = "totalizatortest.sql";

    public static void create(Statement statement) throws IOException, SQLException {
        List<String> sqlCreateTestdb = Files.lines(Paths.get(filename), StandardCharsets.UTF_8).collect(Collectors.toList());

        String scriptCreateDb = "";
        for (String s : sqlCreateTestdb) {
            scriptCreateDb += s + " ";
        }

        String[] lines = scriptCreateDb.split(";");

        for (int i = 0; i < lines.length - 1; i++) {
            String s = lines[i] + ";";
            statement.execute(s + ";");
        }
    }

    public static void drop(Statement statement) throws SQLException {
        statement.execute("DROP SCHEMA IF EXISTS `totalizatorTest`;");
    }

}
