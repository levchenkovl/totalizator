package by.levchenko;

import by.levchenko.database.connectionpool.ConnectionPool;
import by.levchenko.database.connectionpool.ProxyConnection;
import by.levchenko.logic.action.SportAction;
import by.levchenko.logic.exception.SportException;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

public class SportActionTest {

    private SportAction sportAction;
    private ProxyConnection connection;
    private ConnectionPool connectionPool;
    private Statement statement;

    @BeforeTest
    public void init() throws InterruptedException, IOException, SQLException {
        connectionPool = ConnectionPool.getInstance(true);
        connection = connectionPool.getConnection();
        statement = connection.createStatement();
        TestDB.create(statement);

        sportAction = new SportAction(connection);
    }

    @Test
    public void findAllTest() {
        int expectedSize = 12;
        int actualSize = sportAction.findAll(false).size();
        Assert.assertEquals(actualSize, expectedSize);
    }

    @Test
    public void updateMatchByIdTest() {
        boolean actual = sportAction.updateMatchById(2, "Football", "owner", "guest", "Tor");
        Assert.assertTrue(actual);
    }

    @Test
    public void findSportsByUserIdTest() {
        int expectedSize = 5;
        int actualSize = sportAction.findSportsByUserId(1).size();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void deleteSportByIdTest() {
        boolean actual = sportAction.deleteSportById(1);
        Assert.assertTrue(actual);
    }

    @Test
    public void addSportTest() throws SportException {
        boolean actual = sportAction.addSport("Tor", "Football", "Owner", "Guest");
        Assert.assertTrue(actual);
    }

    @AfterTest
    public void dropSchema() throws SQLException {
        connectionPool.closeConnection(connection);
        TestDB.drop(statement);
        statement.close();
    }
}
